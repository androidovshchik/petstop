const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.onmsg = functions.https.onRequest((request, response) => {
	
console.log('request.query', request.query);

 var xml = request.query.body;
 
 if(xml == null || xml == '') {
	response.status(200).send('');
	return;
 }
 
 var token = xml.match(/token>(.*)</)[1];
 var userId = xml.match(/reference>(.*)</)[1];
 if(token != null && userId != null) {
	admin.database().ref('/users').child(userId).child('cardToken').set(token);
 }
 
 var transfer_code = xml.match(/transfer_code>(.*)</)[1];
 var zooUuid = xml.match(/reference>(.*)</)[1];
 var orderId = xml.match(/order_id>(.*)</)[1];
  
 if(transfer_code == null || zooUuid == null || orderId == null) {
	response.status(200).send('');
	return;
 }
 
  console.log('params', {transfer_code: transfer_code, zooUuid:zooUuid, orderId:orderId});
 
  admin.database().ref('/transfer_code').child(zooUuid+''+orderId).set(transfer_code);
  
  response.status(200).send('');
	
});


exports.positionRecalc = functions.database.ref('/users/{userUuid}/top1PaymentDate')
    .onWrite(e => {
		function toInt(xxx){
			return xxx === undefined ? 0 : (xxx === null ? 0 : Number(xxx));
		}
		
		console.log('e.params.userUuid = ' + e.params.userUuid);
		return admin.database().ref('/users').child(''+e.params.userUuid).child('city').once('value').then(function(snapshot) {
		  var city = snapshot.val();
			console.log('city = ' + city);
		  return admin.database().ref('/users').orderByChild('city').equalTo(city).once('value').then(function(snapshot1) {
			var values = snapshot1.val();
			var users = [], k = 0;
		
			for (var val in values) {
				users[k++] = values[val];
			}
			  
			console.log('users = ' + JSON.stringify(users));
			
			users.sort(function(x,y){
				return toInt(y.top1PaymentDate) - toInt(x.top1PaymentDate);
			});
			
			var usersMap = {};

			var updatedUserData = {};
			for(var i = 0; i < users.length; i++){
				usersMap[users[i].uuid] = i+1;
				updatedUserData['users/'+users[i].uuid+'/position'] = i+1;
			}
			  
			console.log('updatedUserData = ' + JSON.stringify(updatedUserData));
			
			// Do a deep-path update
			admin.database().ref().update(updatedUserData, function(error) {
			  if (error) {
				console.log("Error updating data:", error);
			  }else {
				console.log("success updating data");
			  }
			});			  
		  });
		});
    });