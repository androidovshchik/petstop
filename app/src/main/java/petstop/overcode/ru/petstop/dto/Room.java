package petstop.overcode.ru.petstop.dto;

import android.support.annotation.Nullable;

import com.github.huajianjiang.expandablerecyclerview.widget.Parent;
import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Room implements Parent<Message> {

	public ArrayList<Message> messages;

	public String customerUuid;

	public String customerName;

	public String zooUuid;

	public String zooName;

	public long counter;

	public transient String newMessage = "";

	public transient boolean enablePrinting = true;

	@Nullable
	public transient String countUnread = null;

	public Room() {}

	public void setup(boolean asCustomer) {
		if (messages == null) {
			messages = new ArrayList<>();
		}
		messages.removeAll(Collections.<Message>singleton(null));
		int countUnread = 0;
		for (int i = 0; i < messages.size(); i++) {
			if (hasUnreadForMe(asCustomer, i)) {
				countUnread++;
			}
		}
		if (countUnread > 9) {
			this.countUnread = "9+";
		} else if (countUnread > 0) {
			this.countUnread = String.valueOf(countUnread);
		} else {
			this.countUnread = null;
		}
		// dialog input
		this.messages.add(new Message());
	}

	public String getUuid(boolean isCustomer) {
		return isCustomer ? customerUuid : zooUuid;
	}

	public String getName(boolean isCustomer) {
		return isCustomer ? customerName : zooName;
	}

	public boolean isTheSame(Room room) {
		return zooUuid.equals(room.zooUuid) && customerUuid.equals(room.customerUuid);
	}

	public boolean isTheSame(boolean isCustomer, String uuid) {
		return !isCustomer && zooUuid.equals(uuid) || isCustomer && customerUuid.equals(uuid);
	}

	public boolean hasUnreadForMe(boolean isCustomer, int position) {
		if (messages.get(position).text == null) {
			return false;
		}
		return isCustomer && !messages.get(position).readByCustomer ||
			!isCustomer && !messages.get(position).readByZoo;
	}

	@Exclude
	public String getKey() {
		return customerUuid + zooUuid;
	}

	@Exclude
	@Nullable
	public Message getLastMessage() {
		return messages.size() > 0 ? messages.get(messages.size() - 1) : null;
	}

	@Exclude
	@Override
	public List<Message> getChildren() {
		return messages;
	}

	@Exclude
	@Override
	public boolean isInitiallyExpandable() {
		return true;
	}

	@Exclude
	@Override
	public boolean isInitiallyExpanded() {
		return false;
	}
}

