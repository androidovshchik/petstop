package petstop.overcode.ru.petstop.hook;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.HashSet;

import petstop.overcode.ru.petstop.service.GetDataFunc;

/**
 * Created by VIP on 02.09.2017.
 */

public class AutocomleteUtil {
	public static void setAutocomplete(AppCompatActivity activity, int autoCompleteId, final GetDataFunc<HashSet<String>, String> getDataFunc) {
		final AutoCompleteTextView autoComplete = (AutoCompleteTextView) activity.findViewById(autoCompleteId);

		autoComplete.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable editable) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			public void onTextChanged(final CharSequence s, int start, int before, int count) {

				Log.d("", "onTextChanged: " + s);

				new AsyncTask<String, Void, HashSet<String>>() {
					@Override
					protected HashSet<String> doInBackground(String... params) {
						HashSet<String> l = null;
						try {
							Log.d("", "getDataFunc call: " + s);

							l = getDataFunc.call(s.toString());
						} catch (Exception e) {
							Log.e("", "error when getCountryData", e);
						}
						return l;
					}

					@Override
					protected void onPostExecute(HashSet<String> countries) {
						final ArrayAdapter<String> countriesAdapter = new ArrayAdapter<>(autoComplete.getContext(), android.R.layout.simple_dropdown_item_1line);

						countriesAdapter.clear();
						if (countries != null)
							for (String listItem : countries) {
								countriesAdapter.add(listItem);
							}

						autoComplete.setAdapter(countriesAdapter);
						countriesAdapter.notifyDataSetChanged();
					}
				}.execute(s.toString());
			}
		});
	}
}
