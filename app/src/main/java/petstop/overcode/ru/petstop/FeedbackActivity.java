package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import petstop.overcode.ru.petstop.service.PayService;
import petstop.overcode.ru.petstop.service.UserService;
import petstop.overcode.ru.petstop.util.U;

public class FeedbackActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);

		findViewById(R.id.goToBackBtnToMainInFeedBack).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		final EditText subject = (EditText) findViewById(R.id.subjectFeedback);
		final EditText body = (EditText) findViewById(R.id.bodyFeedback);

		final Activity activity = this;
		findViewById(R.id.sendFeedbackBtn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (subject.getText().toString().trim().isEmpty() || body.getText().toString().trim().isEmpty())
					return;
				U.startLoader(activity, "Отправка");
				AsyncTask.execute(new Runnable() {
					@Override
					public void run() {
						PayService.sendMailToAdmin(UserService.currentUser.getEmail(),
							subject.getText().toString(), body.getText().toString());

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								U.finishLoader();
								U.longToast(activity, "Письмо отправлено");
								activity.finish();
							}
						});
					}
				});
			}
		});
	}
}
