package petstop.overcode.ru.petstop;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.util.AuthUtil;
import petstop.overcode.ru.petstop.util.U;

public class LoginActivity extends AppCompatActivity {
	private EditText mEmailView;
	private EditText mPasswordView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (AuthUtil.redirectToMainActivityIfLogged(this)) return;

		setContentView(R.layout.activity_login);
		mEmailView = (EditText) findViewById(R.id.email);

		TextView forgetPasswordBtn = (TextView) findViewById(R.id.forget_password_btn);
		forgetPasswordBtn.setPaintFlags(forgetPasswordBtn.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		forgetPasswordBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), RecoveryPasswordActivity.class);
				startActivity(intent);
			}
		});

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
				if (id == R.id.login || id == EditorInfo.IME_NULL) {
					attemptLogin();
					return true;
				}
				return false;
			}
		});

		Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
		mEmailSignInButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				attemptLogin();
			}
		});
	}

	private void attemptLogin() {
		if (AuthUtil.redirectToMainActivityIfLogged(this)) return;

		mEmailView.setError(null);
		mPasswordView.setError(null);

		final String email = mEmailView.getText().toString();
		String password = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			mPasswordView.setError("Неверный пароль");
			focusView = mPasswordView;
			cancel = true;
		}

		if (TextUtils.isEmpty(email)) {
			mEmailView.setError("Поле, обязательное для заполнения");
			focusView = mEmailView;
			cancel = true;
		} else if (!isEmailValid(email)) {
			mEmailView.setError("Неверный email");
			focusView = mEmailView;
			cancel = true;
		}

		final LoginActivity activity = this;
		if (cancel) {
			focusView.requestFocus();
		} else {
			U.startLoader(this, "Вход");
			AuthUtil.signIn(email, password, new OnDataCallback<Void>(this) {
				@Override
				public void action(Void data) {
					U.finishLoader();
					AuthUtil.redirectToMainActivityIfLogged(activity);
				}

				@Override
				public void fail(String msg) {
					U.finishLoader();
					mEmailView.setError(msg);
				}
			});
		}
	}

	private boolean isEmailValid(String email) {
		return email.contains("@");
	}

	private boolean isPasswordValid(String password) {
		return password.length() > 0;
	}
}

