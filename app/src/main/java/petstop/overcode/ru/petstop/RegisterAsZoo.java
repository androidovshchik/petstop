package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import petstop.overcode.ru.petstop.dto.AnimalType;
import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.hook.AutocomleteUtil;
import petstop.overcode.ru.petstop.service.GetDataFunc;
import petstop.overcode.ru.petstop.service.LocationService;
import petstop.overcode.ru.petstop.service.MetroService;
import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.service.PayService;
import petstop.overcode.ru.petstop.service.UserService;
import petstop.overcode.ru.petstop.util.AuthUtil;
import petstop.overcode.ru.petstop.util.CustomHint;
import petstop.overcode.ru.petstop.util.ImageHelper;
import petstop.overcode.ru.petstop.util.U;

public class RegisterAsZoo extends AppCompatActivity {
	static int SELECT_IMAGE = 7;
	EditText email_input;
	EditText password_input;
	EditText repeat_password_input;
	EditText name_input;
	EditText phone_input;
	Spinner days_spinner;
	Spinner price_spinner;

	ImageView avatarView;
	boolean avatarIsChanged = false;

	AutoCompleteTextView city_autocomplete;
	AutoCompleteTextView street_autocomplete;
	AutoCompleteTextView metro_autocomplete;

	AnimalType[] animalType = new AnimalType[]{AnimalType.All};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_as_zoo);

		findViewById(R.id.toolbarInProfile).setVisibility(View.INVISIBLE);

		final Activity thisActivity = this;


		email_input = (EditText) findViewById(R.id.email_input);
		password_input = (EditText) findViewById(R.id.password_input);
		repeat_password_input = (EditText) findViewById(R.id.repeat_password_input);
		name_input = (EditText) findViewById(R.id.name_input);
		phone_input = (EditText) findViewById(R.id.phone_input);

		days_spinner = (Spinner) findViewById(R.id.days_spinner);
		price_spinner = (Spinner) findViewById(R.id.price_spinner);
		findViewById(R.id.these_data_are_needed_to_communicate_with_the_zoo).setVisibility(View.INVISIBLE);

		CustomHint customHint = new CustomHint("Введите адрес электронной почты", Typeface.ITALIC, 35f);
		email_input.setHint(customHint);
		avatarView = (ImageView) findViewById(R.id.avatar);
		avatarView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(Intent.createChooser(intent, "Выбор изображения"), SELECT_IMAGE);
			}
		});


		city_autocomplete = (AutoCompleteTextView) findViewById(R.id.city_autocomplete);
		street_autocomplete = (AutoCompleteTextView) findViewById(R.id.street_autocomplete);
		metro_autocomplete = (AutoCompleteTextView) findViewById(R.id.metro_autocomplete);

		final Map<AnimalType, View> animalBtns = new HashMap<AnimalType, View>() {{
			put(AnimalType.All, findViewById(R.id.all_btn));
			put(AnimalType.Dog, findViewById(R.id.dog_btn));
			put(AnimalType.Cat, findViewById(R.id.cat_btn));
		}};
		FiltersActivity.addBehavior(animalType, animalBtns);
		findViewById(R.id.all_btn).callOnClick();

		if (AuthUtil.getUser() != null) {
			UserService.getRectAvatar(this, AuthUtil.getUser().getUid(), new OnDataCallback<Bitmap>(this) {
				@Override
				public void action(Bitmap bitmap) {
					if (bitmap == null) return;
					avatarView.setImageBitmap(bitmap);
				}
			});

			MyUser user = UserService.currentUser;
			if (user == null) return;
			if (user.getName() != null) name_input.setText(user.getName());
			if (user.getPhone() != null) phone_input.setText(user.getPhone());
			if (user.getMinDays() > 0)
				days_spinner.setSelection(((ArrayAdapter) days_spinner.getAdapter()).getPosition("" + user.getMinDays()));
			if (user.getPrice() > 0)
				price_spinner.setSelection(((ArrayAdapter) price_spinner.getAdapter()).getPosition("" + user.getPrice()));
			if (user.getCity() != null) city_autocomplete.setText(user.getCity());
			if (user.getStreet() != null) street_autocomplete.setText(user.getStreet());
			if (user.getMetro() != null) metro_autocomplete.setText(user.getMetro());
			animalType[0] = user.getAnimalType();
			animalBtns.get(animalType[0]).callOnClick();
		}

		AutocomleteUtil.setAutocomplete(this, R.id.city_autocomplete, new GetDataFunc<HashSet<String>, String>() {
			@Override
			public HashSet<String> call(String txt) {
				return LocationService.getCityList(txt);
			}
		});
		AutocomleteUtil.setAutocomplete(this, R.id.street_autocomplete, new GetDataFunc<HashSet<String>, String>() {
			@Override
			public HashSet<String> call(String txt) {
				return LocationService.getStreetList(city_autocomplete.getText().toString(), txt);
			}
		});
		AutocomleteUtil.setAutocomplete(this, R.id.metro_autocomplete, new GetDataFunc<HashSet<String>, String>() {
			@Override
			public HashSet<String> call(String txt) {
				return new HashSet<>(MetroService.getMetro(city_autocomplete.getText().toString(), txt));
			}
		});

		Button signup_btn = (Button) findViewById(R.id.signup_btn);
		signup_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				MyUser user = getUser();
				if (!checkErrors(user)) return;

				buttonClick(user);
			}
		});

		if (isEdit()) {
			findViewById(R.id.changePasswordBtn).setVisibility(View.VISIBLE);
			findViewById(R.id.changePasswordBtn).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					U.startLoader(thisActivity, "Отправка письма для сброса пароля");
					AuthUtil.resetPassword(AuthUtil.getUser().getEmail(), new OnDataCallback<Void>(thisActivity) {
						@Override
						public void action(Void data) {
							U.finishLoader();
							Toast.makeText(thisActivity, "Ссылка для сброса пароля отправлена на Ваш почтовый ящик", Toast.LENGTH_LONG).show();
						}
					});
				}
			});
			U.rm(findViewById(R.id.acceptLicenseLayout));
		} else {
			findViewById(R.id.changePasswordBtn).setVisibility(View.GONE);

			TextView acceptLicenseTxt = (TextView) findViewById(R.id.acceptLicenseTxt);
			acceptLicenseTxt.setPaintFlags(acceptLicenseTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
			acceptLicenseTxt.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					String url = PayService.url + "License.txt";
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(url));
					startActivity(i);
				}
			});
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SELECT_IMAGE) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					try {
						Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
						bitmap = ImageHelper.getRectBitmap(this, bitmap);
						avatarView.setImageBitmap(bitmap);
						avatarIsChanged = true;
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
				Toast.makeText(this, "Отменено", Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void uploadImageIfChanged() {
		if (avatarIsChanged) {
			Bitmap bitmap = ((BitmapDrawable) avatarView.getDrawable()).getBitmap();
			UserService.uploadAvatar(bitmap);
		}
	}

	public void buttonClick(final MyUser user) {
		final AppCompatActivity activity = this;

		U.startLoader(activity, "Регистрация");
		AuthUtil.createAccount(user, new OnDataCallback<Void>(activity) {
			@Override
			public void action(Void data) {
				AuthUtil.signIn(user.getEmail(), user.getPassword(), new OnDataCallback<Void>(activity) {
					@Override
					public void action(Void data) {
						uploadImageIfChanged();
						UserService.currentUser.setUuid(AuthUtil.getUser().getUid());
						U.finishLoader();
						AuthUtil.redirectToMainActivityIfLogged(activity);
					}

					@Override
					public void fail(String msg) {
						U.finishLoader();
						email_input.setError("Ошибка сервера");
					}
				});
			}

			@Override
			public void fail(String msg) {
				U.finishLoader();
				email_input.setError("Ошибка сервера");
			}
		});
	}

	public boolean checkErrors(MyUser user) {
		if (email_input != null && !U.isEmailValid(user.getEmail())) {
			email_input.setError("Введите email");
			return false;
		}

		if (password_input != null && user.getPassword().isEmpty()) {
			password_input.setError("Введите пароль");
			return false;
		}

		if (password_input != null && user.getPassword().length() < 6) {
			password_input.setError("Длина пароля не менее 6 символов");
			return false;
		}

		if (repeat_password_input != null && !user.getPassword().equals(repeat_password_input.getText().toString().trim())) {
			repeat_password_input.setError("Пароли не совпадают");
			return false;
		}

		if (phone_input != null && (user.getPhone()).isEmpty()) {
			phone_input.setError("Не указан номер телефона");
			return false;
		}

		if (phone_input != null && !U.isPhoneValid(user.getPhone())) {
			phone_input.setError("Неправильно указан номер");
			return false;
		}

		CheckBox acceptLicenseCheckBox = (CheckBox) findViewById(R.id.acceptLicenseCheckBox);
		if (acceptLicenseCheckBox != null && !acceptLicenseCheckBox.isChecked() && !isEdit()) {
			acceptLicenseCheckBox.setError("Примите лицензионное соглашение");
			return false;
		}

		return true;
	}

	public MyUser getUser() {
		final String name = name_input.getText().toString().trim();
		final String phone = phone_input.getText().toString().trim();
		final String email = email_input == null ? null : email_input.getText().toString().trim();
		final String password = password_input == null ? null : password_input.getText().toString().trim();

		MyUser user = UserService.currentUser == null ? UserService.currentUser = new MyUser() : UserService.currentUser;
		user.setCity(city_autocomplete.getText().toString());
		if (metro_autocomplete != null) user.setMetro(metro_autocomplete.getText().toString());
		if (street_autocomplete != null) user.setStreet(street_autocomplete.getText().toString());
		if (email != null) user.setEmail(email);
		user.setName(name);
		user.setPhone(phone);
		if (password != null) user.setPassword(password);
		if (days_spinner != null)
			user.setMinDays(Integer.parseInt(days_spinner.getSelectedItem().toString()));
		if (price_spinner != null)
			user.setPrice(Integer.parseInt(price_spinner.getSelectedItem().toString()));
		user.setIsZoo(isZoo());
		user.setAnimalType(animalType[0]);

		return user;
	}

	public boolean isZoo() {
		return true;
	}

	public boolean isEdit() {
		return false;
	}
}
