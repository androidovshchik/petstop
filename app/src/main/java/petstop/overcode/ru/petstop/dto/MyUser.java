package petstop.overcode.ru.petstop.dto;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by VIP on 29.07.2017.
 */

public class MyUser implements Serializable {
	private String uuid;
	private boolean isZoo;
	private String cardToken;
	private String phone;
	private String email;
	private String password;
	private String name;
	private String city;
	private String street;
	private String metro;
	private String instagramm;
	private String vkontakte;
	private int price;
	private int position;
	private long top1PaymentDate;
	private int minDays;
	private AnimalType animalType;
	private boolean zooIsLocked;
	private List<ZooBooking> bookings = new ArrayList<>();

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Exclude
	public boolean isCustomer() {
		return !getIsZoo();
	}

	public boolean getIsZoo() {
		return isZoo;
	}

	public void setIsZoo(boolean zoo) {
		isZoo = zoo;
	}

	public String getPhone() {
		return phone == null ? "" : phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Exclude
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name == null ? "" : name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city == null ? "" : city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street == null ? "" : street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getMetro() {
		return metro == null ? "" : metro;
	}

	public void setMetro(String metro) {
		this.metro = metro;
	}

	public String getInstagramm() {
		return instagramm == null ? "" : instagramm;
	}

	public void setInstagramm(String instagramm) {
		this.instagramm = instagramm;
	}

	public String getVkontakte() {
		return vkontakte == null ? "" : vkontakte;
	}

	public void setVkontakte(String vkontakte) {
		this.vkontakte = vkontakte;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getMinDays() {
		return minDays;
	}

	public void setMinDays(int minDays) {
		this.minDays = minDays;
	}

	/*
		Количество выполненных работ
	 */
	public int booksPerformed() {
		return getBookings().size() - activeBookingCustomerUuids().size();
	}

	public int rating() {
		double rating = 0;
		int count = 0;
		for (ZooBooking b : getBookings()) {
			if (b.getRating() >= 0) {
				rating += b.getRating();
				count++;
			}
		}
		return (int) (rating / count);
	}

	@Exclude
	public ZooBooking getActiveBookingForUser(String userUuid) {
		List<ZooBooking> bookings = getBookings();
		for (ZooBooking b : bookings) {
			if (b.getIsActive() && b.getCustomerId().equals(userUuid)) {
				return b;
			}
		}
		return null;
	}

	public AnimalType getAnimalType() {
		return animalType == null ? AnimalType.All : animalType;
	}

	public void setAnimalType(AnimalType animalType) {
		this.animalType = animalType;
	}

	public List<ZooBooking> getBookings() {
		return bookings;
	}

	public void setBookings(List<ZooBooking> bookings) {
		this.bookings = bookings;
	}

	public boolean getZooIsLocked() {
		return zooIsLocked;
	}

	public void setZooIsLocked(boolean zooIsLocked) {
		this.zooIsLocked = zooIsLocked;
	}

	public String getCardToken() {
		return cardToken;
	}

	public void setCardToken(String cardToken) {
		this.cardToken = cardToken;
	}

	public long getTop1PaymentDate() {
		return top1PaymentDate;
	}

	public void setTop1PaymentDate(long top1PaymentDate) {
		this.top1PaymentDate = top1PaymentDate;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	@Exclude
	public String getFullAddress() {
		String address = "";
		if (getCity() != null) {
			address = "г. " + getCity();
			if (getStreet() != null) {
				address = address + ", ул. " + getStreet();
			}
		}
		return address;
	}

	public List<String> activeBookingCustomerUuids() {
		List<String> uuids = new ArrayList<>();
		for (ZooBooking b : bookings) {
			if (b.getIsActive()) {
				uuids.add(b.getCustomerId());
			}
		}
		return uuids;
	}

	@Override
	public String toString() {
		return "MyUser{" +
			"uuid='" + uuid + '\'' +
			", isZoo=" + isZoo +
			", cardToken='" + cardToken + '\'' +
			", phone='" + phone + '\'' +
			", email='" + email + '\'' +
			", password='" + password + '\'' +
			", name='" + name + '\'' +
			", city='" + city + '\'' +
			", street='" + street + '\'' +
			", metro='" + metro + '\'' +
			", instagramm='" + instagramm + '\'' +
			", vkontakte='" + vkontakte + '\'' +
			", price=" + price +
			", position=" + position +
			", top1PaymentDate=" + top1PaymentDate +
			", minDays=" + minDays +
			", animalType=" + animalType +
			", zooIsLocked=" + zooIsLocked +
			", bookings=" + bookings +
			'}';
	}
}
