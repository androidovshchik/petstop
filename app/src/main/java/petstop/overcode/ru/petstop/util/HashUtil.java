package petstop.overcode.ru.petstop.util;

import android.text.TextUtils;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

/**
 * Created by VIP on 20.09.2017.
 */

public class HashUtil {


	public static String Sign(Object... arr) {
		try {
			String str = TextUtils.join("", arr);
			String md5 = HashUtil.md5(str);
			String base64 = HashUtil.base64(md5).trim();
			return base64;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String md5(String message) throws Exception {
		String digest = null;
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] hash = md.digest(message.getBytes("UTF-8"));
		StringBuilder sb = new StringBuilder(2 * hash.length);

		for (byte b : hash) {
			sb.append(String.format("%02x", b & 0xff));
		}

		digest = sb.toString();
		return digest;
	}

	public static String base64(String s) {
		byte[] data = new byte[0];

		try {
			data = s.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} finally {
			String base64Encoded = Base64.encodeToString(data, Base64.URL_SAFE);

			return base64Encoded;
		}
	}
}
