package petstop.overcode.ru.petstop.dto;

import java.io.Serializable;

/**
 * Created by VIP on 25.08.2017.
 */

public class FilterDto implements Serializable {
	private String city;
	private String street;
	private String metro;
	private int priceMin;
	private int priceMax;
	private int minRating;
	private AnimalType animalType;
	private int minDays;

	public String getCity() {
		return city == null ? "" : city.trim();
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street == null ? "" : street.trim();
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getMetro() {
		return metro == null ? "" : metro.trim();
	}

	public void setMetro(String metro) {
		this.metro = metro;
	}

	public int getPriceMin() {
		return Math.max(priceMin, 0);
	}

	public void setPriceMin(int priceMin) {
		this.priceMin = priceMin;
	}

	public int getPriceMax() {
		return priceMax == 0 ? 2000 : priceMax;
	}

	public void setPriceMax(int priceMax) {
		this.priceMax = priceMax;
	}

	public int getMinRating() {
		return minRating;
	}

	public void setMinRating(int minRating) {
		this.minRating = minRating;
	}

	public AnimalType getAnimalType() {
		return animalType == null ? AnimalType.All : animalType;
	}

	public void setAnimalType(AnimalType animalType) {
		this.animalType = animalType;
	}

	public int getMinDays() {
		return minDays;
	}

	public void setMinDays(int minDays) {
		this.minDays = minDays;
	}
}
