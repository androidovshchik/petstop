package petstop.overcode.ru.petstop.service;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by VIP on 19.09.2017.
 */

public interface IPayService {

	@GET("P2PDebit.php")
	public Call<String> P2PDebit(@Query("id") String orderId,
								 @Query("token") String token, @Query("get_token") int get_token);

	@GET("P2PComplete.php")
	public Call<String> P2PComplete(@Query("id") String orderId,
									@Query("token") String token, @Query("transfer_code") String transfer_code,
									@Query("action") String action);

	@GET("Purchase.php")
	public Call<String> Purchase(@Query("id") String orderId,
								 @Query("token") String token, @Query("get_token") int get_token);

	@GET("Operation.php")
	public Call<String> Operation(@Query("id") String orderId,
								  @Query("operation") String operationId, @Query("get_token") int getToken);

	@GET("PurchaseOperation.php")
	public Call<String> PurchaseOperation(@Query("id") String orderId,
										  @Query("operation") String operationId, @Query("get_token") int getToken);

	@GET("Register.php")
	public Call<String> Register(@Query("amount") int amount,
								 @Query("currency") int currency, @Query("email") String email,
								 @Query("description") String description, @Query("mode") int mode,
								 @Query("reference") String reference,
								 @Query("url") String url);

	@GET("PurchaseRegister.php")
	public Call<String> PurchaseRegister(@Query("amount") int amount,
										 @Query("currency") int currency, @Query("email") String email,
										 @Query("description") String description, @Query("mode") int mode,
										 @Query("reference") String reference,
										 @Query("url") String url);

	@GET("Authorize.php")
	public Call<String> Authorize(@Query("id") int orderId, @Query("get_token") String get_token);

	@GET("Push.php")
	public Call<String> PushInterest(@Query("tokens") String tokens, @Query("name") String name);

	@GET("Push.php")
	public Call<String> PushMessage(@Query("uuid") String uuid, @Query("name") String name,
									@Query("tokens") String tokens);

	@GET("Reverse.php")
	public Call<String> Reverse(@Query("id") String orderId, @Query("amount") int amount, @Query("currency") int currency);

}
