package petstop.overcode.ru.petstop.dto;


public class Message {

	public String text;
	public boolean readByCustomer;
	public boolean readByZoo;
	public boolean writtenByZoo;
	public long time;
}
