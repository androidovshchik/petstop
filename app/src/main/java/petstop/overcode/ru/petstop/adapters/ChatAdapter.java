package petstop.overcode.ru.petstop.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.huajianjiang.expandablerecyclerview.widget.ChildViewHolder;
import com.github.huajianjiang.expandablerecyclerview.widget.ExpandableAdapter;
import com.github.huajianjiang.expandablerecyclerview.widget.ParentViewHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Optional;
import petstop.overcode.ru.petstop.R;
import petstop.overcode.ru.petstop.dto.Message;
import petstop.overcode.ru.petstop.dto.Room;
import petstop.overcode.ru.petstop.service.ChatService;
import petstop.overcode.ru.petstop.service.FCMTokenService;
import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.service.PayService;

public class ChatAdapter extends ExpandableAdapter<ChatAdapter.DialogViewHolder,
    ChatAdapter.MessageViewHolder, Room, Message> {

    private LayoutInflater inflater;

    @SuppressWarnings("all")
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final int asRead = Color.parseColor("#eeeeee");
    private final int asUnread = Color.parseColor("#A3A4A4");

    private boolean isCustomer;

    @Nullable
    private String customerUuid;

    @Nullable
    private String customerName;

    @Nullable
    private String zooUuid;

    @Nullable
    private String zooName;

    public ChatAdapter(Context context, @Nullable String zooUuid, @Nullable String zooName, @Nullable String customerUuid,
                       @Nullable String customerName, @Nullable ArrayList<Room> rooms, boolean isCustomer) {
        super(rooms != null ? rooms : new ArrayList<Room>());
        inflater = LayoutInflater.from(context);
        this.zooUuid = zooUuid;
        this.zooName = zooName;
        this.customerUuid = customerUuid;
        this.customerName = customerName;
        this.isCustomer = isCustomer;
    }

    @Override
    public DialogViewHolder onCreateParentViewHolder(ViewGroup parent, int parentType) {
        View itemView = inflater.inflate(parentType, parent, false);
        return new DialogViewHolder(itemView);
    }

    @Override
    public MessageViewHolder onCreateChildViewHolder(ViewGroup child, int childType) {
        View itemView = inflater.inflate(childType, child, false);
        return new MessageViewHolder(itemView);
    }

    @Override
    public void onBindParentViewHolder(DialogViewHolder pvh, int parentPosition) {
        Room room = getParent(parentPosition);
        pvh.name.setText(room.getName(!isCustomer));
        pvh.count.setVisibility(room.countUnread == null ? View.INVISIBLE : View.VISIBLE);
        if (room.countUnread != null) {
            pvh.count.setText(room.countUnread);
        }
    }

    @Override
    public void onBindChildViewHolder(MessageViewHolder cvh, int parentPosition, int childPosition) {
        Message message = getChild(parentPosition, childPosition);
        if (cvh.input == null) {
            Room room = getParent(parentPosition);
            if (isCustomer) {
                cvh.container.setBackgroundColor(!message.readByCustomer ? asUnread : asRead);
                cvh.container.setGravity(message.writtenByZoo ? Gravity.END : Gravity.START);
            } else {
                cvh.container.setBackgroundColor(!message.readByZoo ? asUnread : asRead);
                cvh.container.setGravity(!message.writtenByZoo ? Gravity.END : Gravity.START);
            }
            cvh.name.setText(room.getName(!message.writtenByZoo));
            cvh.text.setText(message.text);
        } else {
            cvh.input.setText(getParent(parentPosition).newMessage);
        }
    }

    @Override
    public int getParentType(int parentPosition) {
        return R.layout.item_dialog_header;
    }

    @Override
    public int getChildType(int parentPosition, int childPosition) {
        if (childPosition < getParent(parentPosition).getChildren().size() - 1) {
            return R.layout.item_chat_message;
        } else {
            return R.layout.item_dialog_input;
        }
    }

    public class DialogViewHolder extends ParentViewHolder<Room> {

        @Nullable
        @BindView(R.id.name)
        public TextView name;
        @Nullable
        @BindView(R.id.count)
        public TextView count;

        public DialogViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class MessageViewHolder extends ChildViewHolder<Message> {

        @Nullable
        @BindView(R.id.input)
        public EditText input;

		@Nullable
		@BindView(R.id.messageContainer)
		public LinearLayout container;
        @Nullable
        @BindView(R.id.name)
        public TextView name;
        @Nullable
        @BindView(R.id.message)
        public TextView text;

        public MessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Optional
        @OnTextChanged(R.id.input)
        protected void onTextChanged(CharSequence text) {
            getParentForAdapterPosition(getAdapterPosition()).newMessage = text.toString();
        }

        @Optional
        @OnClick(R.id.send)
        protected void onSend() {
        	if (getParentForAdapterPosition(getAdapterPosition()).newMessage.trim().isEmpty() ||
                !getParentForAdapterPosition(getAdapterPosition()).enablePrinting) {
        		return;
			}
            getParentForAdapterPosition(getAdapterPosition()).enablePrinting = false;
            final Room room = getParentForAdapterPosition(getAdapterPosition());
            if (ChatAdapter.this.customerUuid != null) {
                room.customerUuid = customerUuid;
                room.customerName = customerName;
            }
            if (ChatAdapter.this.zooUuid != null) {
                room.zooUuid = zooUuid;
                room.zooName = zooName;
            }
            room.counter = 0;
            Message newMessage = new Message();
            newMessage.writtenByZoo = !isCustomer;
            newMessage.readByCustomer = isCustomer;
            newMessage.readByZoo = !isCustomer;
            newMessage.text = room.newMessage;
            newMessage.time = System.currentTimeMillis();
            room.messages.add(newMessage);
            ChatService.addMessage(room, new OnDataCallback<Void>(null) {
				@Override
				public void action(Void data) {
                    getParentForAdapterPosition(getAdapterPosition()).enablePrinting = true;
                    if (isDone) {
                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                FCMTokenService.getTokens(room.getUuid(!isCustomer),
                                    new OnDataCallback<ArrayList<String>>(null) {
                                        @Override
                                        public void action(ArrayList<String> data) {
                                            if (data.size() > 0) {
                                                PayService.PushMessage(room.getUuid(isCustomer),
                                                    data, room.getName(isCustomer));
                                            }
                                        }
                                    });
                            }
                        });
                    }
				}
			});
        }
    }

    @SuppressWarnings("unused")
    public static Context getApplicationContext(View itemView) {
        return itemView.getContext().getApplicationContext();
    }

    @SuppressWarnings("unused")
    public static String getString(View itemView, @StringRes int id, Object... params) {
        return itemView.getContext().getString(id, params);
    }
}
