package petstop.overcode.ru.petstop;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.support.multidex.MultiDexApplication;

import timber.log.Timber;

/**
 * Developer: Vlad Kalyuzhnyu
 * Email:     vladkalyuzhnyu@gmail.com
 */
public class PetStop extends MultiDexApplication {

	@Override
	public void onCreate() {
		super.onCreate();
		if (BuildConfig.DEBUG) {
			Timber.plant(new Timber.DebugTree());
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			String channelId = getString(R.string.default_notification_channel_id);
			String channelName = getString(R.string.default_notification_channel_name);
			NotificationManager notificationManager = getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(new NotificationChannel(channelId,
				channelName, NotificationManager.IMPORTANCE_DEFAULT));
		}
	}
}
