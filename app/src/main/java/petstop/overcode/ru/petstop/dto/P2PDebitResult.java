package petstop.overcode.ru.petstop.dto;

import java.io.Serializable;

/**
 * Created by VIP on 09.10.2017.
 */

public class P2PDebitResult implements Serializable {
	String operationId;
	String orderId;
	boolean isDone;

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public boolean getIsDone() {
		return isDone;
	}

	public void setIsDone(boolean done) {
		isDone = done;
	}
}
