package petstop.overcode.ru.petstop.service;

/**
 * Created by VIP on 25.08.2017.
 */

public abstract class GetDataCallback<TIn, TOut> {
	public abstract TOut get(TIn in);
}
