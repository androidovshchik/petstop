package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.service.UserService;
import petstop.overcode.ru.petstop.util.AuthUtil;
import petstop.overcode.ru.petstop.util.U;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

	public static Context publicContext;
	private View menuLayout;
	private NavigationView navigationView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		publicContext = this;
	}

	@Override
	public void setContentView(int layoutResID) {
		DrawerLayout fullView = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_base, null);

		FrameLayout activityContainer = fullView.findViewById(R.id.activity_content);
		getLayoutInflater().inflate(layoutResID, activityContainer, true);

		super.setContentView(fullView);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		setTitle("Activity Title");

		navigationView = (NavigationView) findViewById(R.id.nav_view);
		View header = navigationView.getHeaderView(0);
		header.findViewById(R.id.closeLeftBarButton).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				DrawerLayout drawler = (DrawerLayout) findViewById(R.id.activity_container);
				drawler.closeDrawers();
			}
		});
		navigationView.setNavigationItemSelectedListener(this);

		menuLayout = navigationView.getHeaderView(0);

		UserService.getRoundedAvatar(this, AuthUtil.getUser().getUid(), new OnDataCallback<Bitmap>(this) {
			@Override
			public void action(Bitmap bitmap) {
				if (bitmap == null) return;
				((ImageView) menuLayout.findViewById(R.id.photoLeftBarImage))
					.setImageBitmap(bitmap);
			}
		});

		UserService.getCurrentUser(new OnDataCallback<MyUser>(this) {
			@Override
			public void action(MyUser user) {

				setText(R.id.nameLeftBarTextView, user.getName());
				setText(R.id.cityLeftBarTextView, user.getCity());

				customizeMenu(navigationView.getMenu());

				if (user.isCustomer()) {
					U.rm(menuLayout.findViewById(R.id.countStartInLeftBar));
					U.rm(menuLayout.findViewById(R.id.completeOrdersTextView));
					U.rm(menuLayout.findViewById(R.id.completeOrdersTextViewNotChange));
					U.rm(menuLayout.findViewById(R.id.positionOnTopLeftBarTextView));
					U.rm(menuLayout.findViewById(R.id.position));
					U.rm(menuLayout.findViewById(R.id.ratingBarLeftBar));
				} else {
					setText(R.id.positionOnTopLeftBarTextView, "" + user.getPosition());
					setText(R.id.countStartInLeftBar, "" + user.rating());
					setText(R.id.completeOrdersTextView, "" + user.booksPerformed());

					RatingBar ratingBar = (RatingBar) menuLayout.findViewById(R.id.ratingBarLeftBar);
					if (ratingBar != null) {
						ratingBar.setRating((float) user.rating());
					}
				}
			}
		});
	}

	@Override
	public void onBackPressed() {
	}

	private void customizeMenu(final Menu menu) {
		MyUser user = UserService.currentUser;

		if (user.getIsZoo()) {
			updateLockStatusForZoo();
			menu.removeItem(R.id.nav_vk);
			menu.removeItem(R.id.nav_instagram);
		} else {
			menu.removeItem(R.id.nav_set_one);
			menu.removeItem(R.id.nav_close_for_order);
		}
	}

	private void setText(int id, String text) {
		TextView view = (TextView) menuLayout.findViewById(id);
		if (view != null) {
			view.setText(text);
		}
	}

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
		int id = item.getItemId();

		MyUser user = UserService.currentUser;
		if (id == R.id.nav_edit_profile) {
			Class activity = user.getIsZoo() ? EditZooProfileActivity.class : EditCustomerProfileActivity.class;
			Intent intent = new Intent(getApplicationContext(), activity);
			startActivity(intent);
		} else if (id == R.id.nav_logout) {
			AuthUtil.logout(getApplicationContext(), new OnDataCallback<Void>(null) {
				@Override
				public void action(Void data) {
					Intent intent = new Intent(getApplicationContext(), EnterActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("EXIT", true);
					startActivity(intent);
					finish();
				}
			});
		} else if (id == R.id.nav_payment_information) {
			startActivity(new Intent(getApplicationContext(), CardInformationActivity.class));
		} else if (id == R.id.nav_chat) {
			startActivity(new Intent(getApplicationContext(), ChatActivity.class));
		} else if (id == R.id.nav_set_one) {
			startActivity(new Intent(getApplicationContext(), SetTopActivity.class));
		} else if (id == R.id.nav_feedback) {
			startActivity(new Intent(getApplicationContext(), FeedbackActivity.class));
		} else if (id == R.id.nav_instagram) {
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/pet_stop_/"));
			startActivity(browserIntent);
		} else if (id == R.id.nav_vk) {
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://vk.com/pet_stop"));
			startActivity(browserIntent);
		} else if (id == R.id.nav_close_for_order) {
			final Activity activity = this;
			U.startLoader(activity, "Обновление статуса");
			UserService.setLockCurrentZoo(!user.getZooIsLocked(), new OnDataCallback<Void>(this) {
				@Override
				public void action(Void data) {
					updateLockStatusForZoo();

					if (activity instanceof ProfileActivity) {
						((ProfileActivity) activity).updateZooInMain();
					}
					U.finishLoader();
				}
			});
			return true;
		}
		return true;
	}

	private void updateLockStatusForZoo() {
		MenuItem item = navigationView.getMenu().findItem(R.id.nav_close_for_order);
		item.setTitle(UserService.currentUser.getZooIsLocked() ? "ОТКРЫТ ДЛЯ ЗАКАЗОВ" : "ЗАКРЫТ ДЛЯ ЗАКАЗОВ");
	}
}
