package petstop.overcode.ru.petstop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class EnterActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("EXIT", false)) {
			finish();
			moveTaskToBack(true);
			System.exit(0);
		}

		setContentView(R.layout.activity_enter);

		Button enterBtn = (Button) findViewById(R.id.enter_btn);
		enterBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
		});
		Button signupBtn = (Button) findViewById(R.id.signup_btn);
		signupBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), RegisterAsActivity.class);
				startActivity(intent);
			}
		});
	}
}
