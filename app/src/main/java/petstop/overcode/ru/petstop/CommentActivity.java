package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;

import java.util.Arrays;
import java.util.Comparator;

import petstop.overcode.ru.petstop.dto.Comment;
import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.dto.ZooBooking;
import petstop.overcode.ru.petstop.service.CommentService;
import petstop.overcode.ru.petstop.service.UserService;

public class CommentActivity extends AppCompatActivity {
	public static Comment lastComment;

	private static ZooBooking getLastBookingForUser(MyUser zoo, String userUuid) {
		ZooBooking[] bookings = zoo.getBookings().toArray(new ZooBooking[zoo.getBookings().size()]);
		Arrays.sort(bookings, new Comparator<ZooBooking>() {
			@Override
			public int compare(ZooBooking a, ZooBooking b) {
				return -((Integer) a.getNumber()).compareTo(b.getNumber());
			}
		});

		for (ZooBooking b : bookings) {
			if (b.getCustomerId().equals(userUuid)) {
				return b;
			}
		}
		throw new RuntimeException("last booking not found!");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comment);

		findViewById(R.id.goToBackBtnComment).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});


		lastComment = null;

		final Activity thisActivity = this;
		findViewById(R.id.notIntrestingButton).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivity(new Intent(thisActivity.getApplicationContext(), MainActivity.class));
			}
		});

		findViewById(R.id.publishButton).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				final String commentTxt = ((EditText) findViewById(R.id.comment_edit)).getText().toString();
				if (commentTxt.isEmpty()) return;

				MyUser currentUser = UserService.currentUser;
				final int rating = (int) ((RatingBar) findViewById(R.id.ratingInComment)).getRating();
				ZooBooking lastBooking = getLastBookingForUser(ProfileActivity.zoo, currentUser.getUuid());
				lastBooking.setRating(rating);

				final Bundle extras = getIntent().getExtras();
				Comment comment = new Comment();
				comment.setFromUserUuid(currentUser.getUuid());
				comment.setUserUuid(extras.getString("user_uuid"));
				comment.setName(currentUser.getName());
				comment.setCity(currentUser.getCity());
				comment.setText(commentTxt);
				comment.setBookingId(Integer.parseInt(extras.getString("booking_id")));
				comment.setRating(rating);
				CommentService.add(comment);

				lastComment = comment;
				startActivity(new Intent(thisActivity.getApplicationContext(), MainActivity.class));
			}
		});
	}
}
