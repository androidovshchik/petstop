package petstop.overcode.ru.petstop.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import petstop.overcode.ru.petstop.EnterActivity;
import petstop.overcode.ru.petstop.MainActivity;
import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.service.FCMTokenService;
import petstop.overcode.ru.petstop.service.Fb;
import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.service.UserService;

/**
 * Created by VIP on 22.08.2017.
 */

public class AuthUtil {
	public static FirebaseAuth getAuth() {
		return FirebaseAuth.getInstance();
	}

	public static FirebaseUser getUser() {
		return getAuth().getCurrentUser();
	}

	public static void resetPassword(final String email, final OnDataCallback<Void> callback) {
		Fb.withTimeout(new Runnable() {
			@Override
			public void run() {

				getAuth().sendPasswordResetEmail(email)
					.addOnCompleteListener(new OnCompleteListener<Void>() {
						@Override
						public void onComplete(@NonNull Task<Void> task) {
							if (task.isSuccessful()) {
								callback.doAction(null);
							} else {
								callback.doFail("Ошибка сервера");
							}
						}
					});
			}
		}, callback);
	}

	public static boolean CheckLogin(AppCompatActivity activity) {
		if (getUser() == null) {
			Intent startApp = new Intent(activity.getApplicationContext(), EnterActivity.class);
			startApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.startActivity(startApp);
			activity.finish();
			return false;
		}
		return true;
	}

	public static boolean redirectToMainActivityIfLogged(final Activity activity) {
		if (FirebaseAuth.getInstance().getCurrentUser() != null) {
			Intent startApp = new Intent(activity.getApplicationContext(), MainActivity.class);
			startApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.startActivity(startApp);
			activity.finish();

			return true;
		}
		return false;
	}

	public static void createAccount(final MyUser user, final OnDataCallback<Void> callback) {
		Fb.withTimeout(new Runnable() {
			@Override
			public void run() {
				getAuth().createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
					.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
						@Override
						public void onComplete(@NonNull Task<AuthResult> task) {
							if (!task.isSuccessful()) {
								callback.doFail("Ошибка сервера");
							} else {
								try {
									user.setUuid(getUser().getUid());
									Fb.ref().child("users").child(user.getUuid()).setValue(user);
									FirebaseInstanceId instanceID = FirebaseInstanceId.getInstance();
									String token = instanceID.getToken();
									if (token != null && !token.trim().isEmpty()) {
										FCMTokenService.addToken(user, token);
									}
								} catch (Exception ee) {
									Log.d("createAccount error", "createAccount error", ee);
									callback.doFail("Ошибка сервера");
								}
								signIn(user.getEmail(), user.getPassword(), callback);
							}
						}
					});
			}
		}, callback);
	}

	public static void signIn(final String email, final String password, final OnDataCallback<Void> callback) {
		Fb.withTimeout(new Runnable() {
			@Override
			public void run() {
				try {
					getAuth().signInWithEmailAndPassword(email, password)
						.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
							@Override
							public void onComplete(@NonNull Task<AuthResult> task) {
								if (task.getException() != null && task.getException().getMessage().toLowerCase().contains("network")) {
									// not setting callback.isDone
									return;
								}
								if (!task.isSuccessful()) {
									callback.doFail("Неверные логин/пароль");
								} else {
									callback.doAction(null);
								}
							}
						});
				} catch (Exception e) {
					Log.d("error", "signIn", e);
					callback.doFail("Неверные логин/пароль");
				}
			}
		}, callback);
	}

	public static void logout(final Context context, final OnDataCallback<Void> callback) {
		FirebaseInstanceId instanceID = FirebaseInstanceId.getInstance();
		String token = instanceID.getToken();
		if (token != null && !token.trim().isEmpty() && UserService.currentUser != null) {
			FCMTokenService.removeToken(UserService.currentUser, token, new OnDataCallback<Void>(null) {
				@Override
				public void action(Void data) {
					getAuth().signOut();
					UserService.currentUser = null;
					if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
						((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE))
							.clearApplicationUserData();
					}
					callback.isDone = true;
					callback.action(null);
				}
			});
		} else {
			getAuth().signOut();
			UserService.currentUser = null;
			if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
				((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE))
					.clearApplicationUserData();
			}
			callback.isDone = true;
			callback.action(null);
		}
	}
}
