package petstop.overcode.ru.petstop;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import petstop.overcode.ru.petstop.dto.Comment;
import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.dto.P2PDebitResult;
import petstop.overcode.ru.petstop.dto.ZooBooking;
import petstop.overcode.ru.petstop.service.CommentService;
import petstop.overcode.ru.petstop.service.FCMTokenService;
import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.service.PayService;
import petstop.overcode.ru.petstop.service.UserService;
import petstop.overcode.ru.petstop.util.U;
import timber.log.Timber;

public class ProfileActivity extends BaseActivity {

	public static MyUser zoo;
	LinearLayout comments_layout;
	ImageView blockImageView;
	Activity thisActivity;
	Button bookOrDoneBtn;
	ImageView callButtonInProfile;

	private static int diffInDays(Date date1, Date date2) {
		return ((int) Math.abs(date1.getTime() - date2.getTime())) / 24 / 60 / 60 / 1000;
	}

	View.OnClickListener getCompleteAction(final MyUser zoo) {
		return new View.OnClickListener() {
			@Override
			public void onClick(final View view) {

				final Dialog[] dialogRef = new Dialog[1];
				final Dialog dialog = new ApplyDialog(thisActivity, new OnDataCallback<Void>(thisActivity) {
					@Override
					public void action(Void data) {
						U.startLoader(activity, "Завершение");
						UserService.completeBooking(zoo, new OnDataCallback<Integer>(thisActivity) {
							@Override
							public void action(Integer bookingId) {
								bookOrDoneBtn.setText("ЗАБРОНИРОВАТЬ");
								bookOrDoneBtn.setOnClickListener(getBookAction());
								updateView();
								U.finishLoader();

								Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
								Bundle b = new Bundle();
								b.putSerializable("user_uuid", zoo.getUuid());
								b.putSerializable("booking_id", bookingId.toString());
								intent.putExtras(b);
								startActivityForResult(intent, 1);
							}
						});
					}
				}, new OnDataCallback<Void>(thisActivity) {
					@Override
					public void action(Void data) {
						dialogRef[0].cancel();
					}
				});
				dialogRef[0] = dialog;
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setCancelable(true);
				dialog.show();
			}
		};
	}

	View.OnClickListener getBookAction() {
		return new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (zoo.getCardToken() == null || zoo.getCardToken().trim().isEmpty() ||
					zoo.getCardToken().trim().equals("null")) {
					final Dialog dialog = new Dialog(thisActivity);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setCancelable(true);
					dialog.setContentView(R.layout.activity_warning_dialog);
					dialog.show();
					AsyncTask.execute(new Runnable() {
						@Override
						public void run() {
							FCMTokenService.getTokens(zoo.getUuid(), new OnDataCallback<ArrayList<String>>(null) {
								@Override
								public void action(ArrayList<String> data) {
									if (data.size() > 0) {
										PayService.PushInterest(zoo, data, UserService.currentUser.getName());
									}
								}
							});
						}
					});
				} else {
					Intent intent = new Intent(getApplicationContext(), BookingActivity.class);
					startActivityForResult(intent, 1);
				}
			}
		};
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		thisActivity = this;
		zoo = (MyUser) getIntent().getExtras().getSerializable("user");
		if (zoo == null) return;

		if (getIntent().hasExtra("name")) {
			Timber.d("onCreate() extra %s", getIntent().getStringExtra("name"));
			final Dialog dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setCancelable(true);
			View view = View.inflate(getApplicationContext(),
				R.layout.activity_notification_dialog, null);
			dialog.setContentView(view);
			dialog.setOnShowListener(new DialogInterface.OnShowListener() {
				@Override
				public void onShow(DialogInterface dialogInterface) {
					CustomTextView textView = dialog.findViewById(R.id.notification_body);
					textView.setText(getString(R.string.notification_body,
						getIntent().getStringExtra("name")));
				}
			});
			dialog.show();
		}

		blockImageView = (ImageView) findViewById(R.id.blockImageView);

		findViewById(R.id.goToBackBtnToMain).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		findViewById(R.id.leftBarImageButtonInProfileAsZoo).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				DrawerLayout drawler = (DrawerLayout) findViewById(R.id.activity_container);
				drawler.openDrawer(Gravity.START);
			}
		});


		final ImageView avatarView = (ImageView) findViewById(R.id.avatarView);
		UserService.getRectAvatar(this, zoo.getUuid(), new OnDataCallback<Bitmap>(thisActivity) {
			@Override
			public void action(Bitmap bitmap) {
				if (bitmap == null) return;
				avatarView.setImageBitmap(bitmap);
			}
		});

		CustomTextView userNameView = (CustomTextView) findViewById(R.id.nameTextViewInProfile);
		userNameView.setText(zoo.getName());

		CustomTextView cityTextViewInProfile = (CustomTextView) findViewById(R.id.cityTextViewInProfile);
		cityTextViewInProfile.setText(zoo.getFullAddress());

		CustomTextView costTextViewInProfile = (CustomTextView) findViewById(R.id.costTextViewInProfile);
		costTextViewInProfile.setText("" + zoo.getPrice());

		((CustomTextView) findViewById(R.id.telTextViewInProfile)).setText(zoo.getPhone());

		CustomTextView daysTextViewInProfile = (CustomTextView) findViewById(R.id.daysTextViewInProfile);
		daysTextViewInProfile.setText("" + zoo.getMinDays());

		CustomTextView ratingTextViewInProfile = (CustomTextView) findViewById(R.id.ratingTextViewInProfile);
		ratingTextViewInProfile.setText("" + zoo.rating());

		CustomTextView countRelizeJobsTextView = (CustomTextView) findViewById(R.id.countRelizeJobsTextView);
		countRelizeJobsTextView.setText("" + zoo.booksPerformed());

		RatingBar ratingBarFromProfile = (RatingBar) findViewById(R.id.ratingBarFromProfile);
		ratingBarFromProfile.setRating((float) zoo.rating());

		if (UserService.currentUser.getIsZoo()) {
			updateZooInMain();
			checkCompleteBookings();
			findViewById(R.id.writeButtonInProfile).setVisibility(View.GONE);
			findViewById(R.id.toolbarInProfileAsCostumer).setVisibility(View.GONE);
			findViewById(R.id.toolbarInProfileAsZoo).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.toolbarInProfileAsCostumer).setVisibility(View.VISIBLE);
			findViewById(R.id.toolbarInProfileAsZoo).setVisibility(View.GONE);

			final Activity thisActivity = this;
			bookOrDoneBtn = findViewById(R.id.bookOrDoneBtn);
			callButtonInProfile = findViewById(R.id.callButtonInProfile);
			findViewById(R.id.writeButtonInProfile).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
					Bundle b = new Bundle();
					b.putSerializable("user", zoo);
					intent.putExtras(b);
					startActivity(intent);
				}
			});
			callButtonInProfile.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (ActivityCompat.checkSelfPermission(thisActivity, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
						ActivityCompat.requestPermissions(thisActivity, new String[]{android.Manifest.permission.CALL_PHONE}, 9);
					} else {
						callPhone();
					}
				}
			});
			updateView();
		}

		comments_layout = findViewById(R.id.comments_layout);
		CommentService.forUser(zoo.getUuid(), new OnDataCallback<List<Comment>>(thisActivity) {
			@Override
			public void action(List<Comment> data) {
				comments_layout.removeAllViews();
				for (Comment comment : data) {
					comments_layout.addView(newItem(comment));
				}

				if (data.isEmpty()) {
					findViewById(R.id.emptyComments).setVisibility(View.VISIBLE);
				} else {
					findViewById(R.id.emptyComments).setVisibility(View.GONE);
				}
			}
		});
	}

	private void checkCompleteBookings() {
		if (zoo.getCardToken() == null) return;
		//
		Toast.makeText(thisActivity, "Проверка завершенных задач", Toast.LENGTH_LONG).show();
		for (final ZooBooking booking : zoo.getBookings()) {
			P2PDebitResult order = booking.getOrder();
			if (order == null || order.getIsDone()) continue;

			final int maxHoldDaysInBank = 7;
			if (booking.getIsActive() && diffInDays(booking.getDate(), new Date()) < Math.min(zoo.getMinDays(), maxHoldDaysInBank))
				continue;

			PayService.completeP2PDebit(zoo.getUuid(), zoo.getCardToken(), booking, new OnDataCallback<Void>(thisActivity) {
				@Override
				public void action(Void data) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(thisActivity, "Счет пополнен", Toast.LENGTH_LONG).show();
						}
					});
				}
			});
		}
	}

	private void callPhone() {
		if (checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE)
			== PackageManager.PERMISSION_GRANTED) {
			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel:" + zoo.getPhone()));
			startActivity(intent);
		}
	}

	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			callPhone();
		}
	}


	public void updateZooInMain() {
		U.rm(findViewById(R.id.bookOrDoneBtn));
		U.rm(findViewById(R.id.callButtonInProfile));

		MyUser zoo = UserService.currentUser;
		if (zoo.getZooIsLocked()) {
			blockImageView.setImageResource(R.drawable.block_256);
			blockImageView.setVisibility(View.VISIBLE);
		} else {
			blockImageView.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		finish();
		Intent intent = getIntent();
		Bundle b = new Bundle();
		b.putSerializable("user", zoo);
		intent.putExtras(b);
		startActivity(intent);
	}

	private void updateView() {
		List<String> activeBookingCustomerUuids = zoo.activeBookingCustomerUuids();
		Timber.d("activeBookingCustomerUuids %s", activeBookingCustomerUuids.toString());
		Timber.d("zoo %s", zoo.toString());
		Timber.d("currentUser %s", UserService.currentUser.toString());
		if (!zoo.getZooIsLocked() && !activeBookingCustomerUuids.contains(UserService.currentUser.getUuid())) {
			blockImageView.setVisibility(View.INVISIBLE);
			bookOrDoneBtn.setText("ЗАБРОНИРОВАТЬ");
			bookOrDoneBtn.setOnClickListener(getBookAction());

			if (!zoo.getUuid().equals(UserService.currentUser.getUuid())) {
				findViewById(R.id.telTextViewInProfile).setVisibility(View.GONE);
				findViewById(R.id.phoneText).setVisibility(View.GONE);
				callButtonInProfile.setVisibility(View.GONE);
			}
		} else {
			if (activeBookingCustomerUuids.contains(UserService.currentUser.getUuid())) {
				blockImageView.setImageResource(R.drawable.mark_256);
				bookOrDoneBtn.setText("ВЫПОЛНЕНО");
				bookOrDoneBtn.setOnClickListener(getCompleteAction(zoo));

				findViewById(R.id.telTextViewInProfile).setVisibility(View.VISIBLE);
				findViewById(R.id.phoneText).setVisibility(View.VISIBLE);
				callButtonInProfile.setVisibility(View.VISIBLE);
			} else {
				blockImageView.setImageResource(R.drawable.block_256);
				U.rm(bookOrDoneBtn);

				findViewById(R.id.telTextViewInProfile).setVisibility(View.GONE);
				findViewById(R.id.phoneText).setVisibility(View.GONE);
				callButtonInProfile.setVisibility(View.GONE);
			}
			blockImageView.setVisibility(View.VISIBLE);
		}
	}

	private LinearLayout newItem(final Comment comment) {
		LinearLayout item = (LinearLayout) getLayoutInflater().inflate(R.layout.item_inprofile, comments_layout, false);
		((TextView) item.findViewById(R.id.nameTextViewInReview)).setText(comment.getName());
		((TextView) item.findViewById(R.id.cityTextViewInReview)).setText(comment.getCity());
		((TextView) item.findViewById(R.id.reviewTextView)).setText(comment.getText());
		((RatingBar) item.findViewById(R.id.ratingBarInReview)).setRating((float) comment.getRating());
		((TextView) item.findViewById(R.id.countStarsTextViewInReview)).setText("" + comment.getRating());
		return item;
	}
}
