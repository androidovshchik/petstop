package petstop.overcode.ru.petstop.dto;

/**
 * Created by VIP on 29.07.2017.
 */

public class Comment {
	private String fromUserUuid;
	private String userUuid;
	private String name;
	private String city;
	private String text;
	private int rating;
	private int bookingId;

	public String getUserUuid() {
		return userUuid;
	}

	public void setUserUuid(String userUuid) {
		this.userUuid = userUuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getFromUserUuid() {
		return fromUserUuid;
	}

	public void setFromUserUuid(String fromUserUuid) {
		this.fromUserUuid = fromUserUuid;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}
}
