package petstop.overcode.ru.petstop;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.service.UserService;

public class EditZooProfileActivity extends RegisterAsZoo {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		findViewById(R.id.toolbarInProfile).setVisibility(View.VISIBLE);
		findViewById(R.id.goToBackBtnToMainInChanger).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		((ViewGroup) email_input.getParent().getParent()).removeView((View) email_input.getParent());
		email_input = null;
		((ViewGroup) password_input.getParent().getParent()).removeView((View) password_input.getParent());
		password_input = null;
		((ViewGroup) repeat_password_input.getParent().getParent()).removeView((View) repeat_password_input.getParent());
		repeat_password_input = null;

		Button signup_btn = (Button) findViewById(R.id.signup_btn);
		signup_btn.setText("СОХРАНИТЬ");
	}

	@Override
	public void buttonClick(final MyUser user) {
		uploadImageIfChanged();
		UserService.updateUserData(user);
		finish();
	}


	@Override
	public boolean isEdit() {
		return true;
	}
}
