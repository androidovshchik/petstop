package petstop.overcode.ru.petstop.service;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import petstop.overcode.ru.petstop.Constants;
import petstop.overcode.ru.petstop.dto.Room;
import petstop.overcode.ru.petstop.dto.Message;
import timber.log.Timber;

public class ChatService {

	private static final GenericTypeIndicator<ArrayList<Room>> ROOMS_INDICATOR =
		new GenericTypeIndicator<ArrayList<Room>>() {};

	public static HashMap<String, Long> counters = new HashMap<>();

	public static void markAsRead(final Room room, final boolean byZoo) {
		Fb.ref().child("chat").addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				ArrayList<Room> rooms = dataSnapshot.getValue(ROOMS_INDICATOR);
				if (rooms != null) {
					rooms.removeAll(Collections.<Room>singleton(null));
				} else {
					rooms = new ArrayList<>();
				}
				boolean hasChanges = false;
				for (int i = 0; i < rooms.size(); i++) {
					if (rooms.get(i).isTheSame(room)) {
						Timber.tag(Constants.TAG_CHAT).d("Mark task: found the same room");
						rooms.get(i).counter++;
						for (int j = 0; j < rooms.get(i).messages.size(); j++) {
							if (byZoo) {
								if (!rooms.get(i).messages.get(j).readByZoo) {
									rooms.get(i).messages.get(j).readByZoo = true;
									hasChanges = true;
								}
							} else {
								if (!rooms.get(i).messages.get(j).readByCustomer) {
									rooms.get(i).messages.get(j).readByCustomer = true;
									hasChanges = true;
								}
							}
						}
						break;
					}
				}
				if (hasChanges) {
					Timber.tag(Constants.TAG_CHAT).d("Mark task: has changes and is done");
					Fb.ref().child("chat").setValue(rooms);
				}
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {}
		});
	}

	public static void addMessage(final Room room, final OnDataCallback<Void> callback) {
		if (room.messages.size() <= 0) {
			Timber.tag(Constants.TAG_CHAT).w("Add message task: empty room messages");
			return;
		}
		Fb.ref().child("chat").addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				ArrayList<Room> rooms = dataSnapshot.getValue(ROOMS_INDICATOR);
				if (rooms != null) {
					rooms.removeAll(Collections.<Room>singleton(null));
				} else {
					rooms = new ArrayList<>();
				}
				for (int i = 0; i < rooms.size(); i++) {
					if (rooms.get(i).isTheSame(room)) {
						rooms.get(i).messages.removeAll(Collections.<Message>singleton(null));
						rooms.get(i).messages.add(room.getLastMessage());
						rooms.get(i).customerUuid = room.customerUuid;
						rooms.get(i).customerName = room.customerName;
						rooms.get(i).zooUuid = room.zooUuid;
						rooms.get(i).zooName = room.zooName;
						rooms.get(i).counter++;
						Timber.tag(Constants.TAG_CHAT).d("Add message task: is done for existing room");
						Fb.ref().child("chat").setValue(rooms);
						callback.isDone = true;
						callback.action(null);
						return;
					}
				}
				Room newRoom = new Room();
				newRoom.messages = new ArrayList<>();
				newRoom.messages.add(room.getLastMessage());
				newRoom.customerUuid = room.customerUuid;
				newRoom.customerName = room.customerName;
				newRoom.zooUuid = room.zooUuid;
				newRoom.zooName = room.zooName;
				newRoom.counter = 0;
				rooms.add(newRoom);
				Timber.tag(Constants.TAG_CHAT).d("Add message task: is done for new room");
				Fb.ref().child("chat").setValue(rooms);
				callback.isDone = true;
				callback.action(null);
			}

			@Override
			public void onCancelled(DatabaseError firebaseError) {
				callback.isDone = false;
				callback.action(null);
			}
		});
	}

	public static void onRooms(DataSnapshot dataSnapshot, boolean asCustomer, String uuid,
								OnDataCallback<ArrayList<Room>> callback) {
		ArrayList<Room> rooms = dataSnapshot.getValue(ROOMS_INDICATOR);
		ArrayList<Room> currentRooms;
		if (rooms != null) {
			rooms.removeAll(Collections.<Room>singleton(null));
			boolean hasUpdates = false;
			for (Map.Entry<String, Long> entry : counters.entrySet()) {
				Timber.tag(Constants.TAG_CHAT).d("Get rooms task: counters: %s %d", entry.getKey(),
					entry.getValue());
			}
			int myRoomsCount = 0;
			for (int i = 0; i < rooms.size(); i++) {
				if (rooms.get(i).isTheSame(asCustomer, uuid)) {
					myRoomsCount++;
					if (!counters.containsKey(rooms.get(i).getKey()) ||
						rooms.get(i).counter > counters.get(rooms.get(i).getKey())) {
						counters.put(rooms.get(i).getKey(), rooms.get(i).counter);
						hasUpdates = true;
					}
				}
			}
			if (myRoomsCount == 0) {
				hasUpdates = true;
			}
			if (!hasUpdates) {
				Timber.tag(Constants.TAG_CHAT).d("Get rooms task: hasn't updates");
				return;
			}
			currentRooms = new ArrayList<>();
			for (int i = 0; i < rooms.size(); i++) {
				if (rooms.get(i).isTheSame(asCustomer, uuid)) {
					rooms.get(i).messages.removeAll(Collections.<Message>singleton(null));
					rooms.get(i).setup(asCustomer);
					currentRooms.add(rooms.get(i));
				}
			}
		} else {
			currentRooms = new ArrayList<>();
		}
		Timber.tag(Constants.TAG_CHAT).d("Get rooms task: is done");
		callback.isDone = true;
		callback.action(currentRooms);
	}
}
