package petstop.overcode.ru.petstop.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.LinkedHashSet;

import petstop.overcode.ru.petstop.util.U;


/**
 * Created by VIP on 01.09.2017.
 */

public class LocationService {
	final static String url = "https://kladr-api.ru/api.php?";
	//final static KladrApiClient client = new KladrApiClient("59aa91e60a69de190e8b4589");

	public static HashSet<String> getCityList(String filter) {
		HashSet<String> strings = new LinkedHashSet<>();

		if (filter == null || filter.isEmpty()) return strings;

		JSONArray cities = fetch(url + "contentType=city&query=" + filter);
		for (int i = 0; i < cities.length(); i++) {
			try {
				strings.add(cities.getJSONObject(i).getString("name"));
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		return strings;
	}

	public static HashSet<String> getStreetList(String city, String filter) {
		HashSet<String> strings = new LinkedHashSet<>();

		if (filter == null || filter.isEmpty()) return strings;

		JSONArray cities = fetch(url + "oneString=1&contentType=street&query=" + "г. " + city + " ул. " + filter);
		for (int i = 0; i < cities.length(); i++) {
			try {
				strings.add(cities.getJSONObject(i).getString("name"));
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		return strings;
	}

	private static JSONArray fetch(String url) {
		String body = U.fetchResponseUrl(url);
		try {
			return new JSONObject(body).getJSONArray("result");
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
