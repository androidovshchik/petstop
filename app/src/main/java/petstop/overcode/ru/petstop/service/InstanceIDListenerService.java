package petstop.overcode.ru.petstop.service;

import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceIdService;

public class InstanceIDListenerService extends FirebaseInstanceIdService {

	@Override
	public void onTokenRefresh() {
		// Fetch updated Instance ID token and notify of changes
		Intent intent = new Intent(this, RegistrationIntentService.class);
		startService(intent);
	}
}
