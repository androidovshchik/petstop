package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.service.PayService;
import petstop.overcode.ru.petstop.service.UserService;
import petstop.overcode.ru.petstop.util.U;

public class SetTopActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_top);

		final MyUser user = UserService.currentUser;
		findViewById(R.id.goToBackBtnTop).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});


		final Activity thisActivity = this;
		U.startLoader(thisActivity, "Загрузка");

		AsyncTask.execute(new Runnable() {
			@Override
			public void run() {
				final String url = PayService.PayForTop(140000, user.getEmail(), user.getUuid(), user.getCardToken());
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						final WebView web = U.initNewWebView(thisActivity, GetOnTokenCallback());
						web.loadUrl(url);
						LinearLayout ll = (LinearLayout) findViewById(R.id.layoutInSetTop);
						ll.addView(web);
					}
				});
			}
		});


	}

	private OnDataCallback<String> GetOnTokenCallback() {
		final Activity thisActivity = this;
		return new OnDataCallback<String>(this) {
			@Override
			public void action(final String url) {
				if (url.contains("my.php")) {
					if (U.isNot0(U.getParam(url, "operation"))) {
						AsyncTask.execute(new Runnable() {
							@Override
							public void run() {
								final String status = PayService.GetPurchaseStatus(U.getParam(url, "id"), U.getParam(url, "operation"));
								if (!"APPROVED".equals(status)) {
									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											U.longToast(thisActivity, "Ошибка оплаты. " + status);
										}
									});
									try {
										Thread.sleep(3500);
									} catch (InterruptedException e) {
									}
									thisActivity.finish();
								} else {
									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											UserService.setTopForUser(UserService.currentUser, new OnDataCallback<Void>(thisActivity) {
												@Override
												public void action(Void data) {
													U.longToast(thisActivity, "Оплачено");
												}
											});
										}
									});
								}
							}
						});


					} else {
						U.longToast(thisActivity, "Ошибка сервиса оплаты");
					}
				} else {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							U.finishLoader();
						}
					});
				}
			}
		};
	}
}
