package petstop.overcode.ru.petstop.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import petstop.overcode.ru.petstop.MainActivity;
import petstop.overcode.ru.petstop.R;
import timber.log.Timber;

public class FCMMessageHandler extends FirebaseMessagingService {

	public static final int MESSAGE_NOTIFICATION_ID = 435345;

	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		Map<String, String> data = remoteMessage.getData();
		Timber.d("onMessageReceived data " + data.toString());
		String from = remoteMessage.getFrom();
		Timber.d("onMessageReceived from " + from);
		createNotification(data);
	}

	private void createNotification(Map<String, String> data) {
		Context context = getBaseContext();
		Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification);
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		if (data.containsKey("name")) {
			intent.putExtra(MainActivity.EXTRA_NAME, data.get("name"));
		}
		if (data.containsKey("uuid")) {
			intent.putExtra(MainActivity.EXTRA_UUID, data.get("uuid"));
		}
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
			getString(R.string.default_notification_channel_id))
			.setSmallIcon(R.drawable.logo_48)
			.setCustomContentView(contentView)
			.setSound(soundUri)
			.setAutoCancel(true)
			.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT));
		Notification notification = builder.build();
		notification.contentView.setTextViewText(R.id.body, data.get("body"));
		NotificationManager mNotificationManager = (NotificationManager) context
			.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, notification);
	}
}