package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.AppCompatTextView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.List;

import petstop.overcode.ru.petstop.dto.AnimalType;
import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.service.FCMTokenService;
import petstop.overcode.ru.petstop.service.Fb;
import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.service.RegistrationIntentService;
import petstop.overcode.ru.petstop.service.UserService;
import petstop.overcode.ru.petstop.util.AuthUtil;
import petstop.overcode.ru.petstop.util.U;
import timber.log.Timber;

public class MainActivity extends BaseActivity {

	public static final String EXTRA_NAME = "name";
	public static final String EXTRA_UUID = "uuid";

	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	LinearLayout performer_items_layout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (checkPlayServices()) {
			Intent intent = new Intent(this, RegistrationIntentService.class);
			startService(intent);
		}

		if (!AuthUtil.CheckLogin(this)) {
			return;
		}

		final Activity activity = this;
		U.startLoader(this, "Загрузка");
		UserService.getCurrentUser(new OnDataCallback<MyUser>(activity) {
			@Override
			public void action(MyUser user) {
				U.finishLoader();
				FirebaseInstanceId instanceID = FirebaseInstanceId.getInstance();
				String token = instanceID.getToken();
				if (token != null && !token.trim().isEmpty()) {
					FCMTokenService.addToken(user, token);
				}
				if (user.getIsZoo()) {
					gotoUserProfile(user);
				} else {
					setContentView(R.layout.activity_main);
					performer_items_layout = findViewById(R.id.performer_items_layout);

					findViewById(R.id.toolbar).findViewById(R.id.openLeftBarInMain).setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							DrawerLayout drawler = findViewById(R.id.activity_container);
							drawler.openDrawer(Gravity.START);
						}
					});

					findViewById(R.id.toolbar).findViewById(R.id.filterBtn).setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							Intent startApp = new Intent(getApplicationContext(), FiltersActivity.class);
							startActivityForResult(startApp, 1);
						}
					});

					LoadZoos();

					if (getIntent().hasExtra(EXTRA_NAME)) {
						Timber.d("action() extra %s", getIntent().getStringExtra(EXTRA_NAME));
						final Dialog dialog = new Dialog(MainActivity.this);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						dialog.setCancelable(true);
						View view = View.inflate(getApplicationContext(),
							R.layout.activity_notification_dialog, null);
						dialog.setContentView(view);
						dialog.setOnShowListener(new DialogInterface.OnShowListener() {
							@Override
							public void onShow(DialogInterface dialogInterface) {
								CustomTextView textView = dialog.findViewById(R.id.notification_body);
								textView.setText(getString(R.string.notification_body,
									getIntent().getStringExtra(EXTRA_NAME)));
							}
						});
						dialog.show();
					}
				}
				if (getIntent().hasExtra(EXTRA_UUID)) {
					Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
					startActivity(intent);
				}
			}
		});
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LoadZoos();
	}

	private void LoadZoos() {
		U.startLoader(this, "Загрузка");
		final Activity activity = this;
		UserService.getZoo(FiltersActivity.filter, new OnDataCallback<List<MyUser>>(this) {
			@Override
			public void action(List<MyUser> data) {
				performer_items_layout.removeAllViews();
				for (MyUser user : data) {
					performer_items_layout.addView(newItem(user));
				}
				if (data.size() == 0) {
					performer_items_layout.addView(new AppCompatTextView(activity) {{
						setPadding(20, 20, 20, 20);
						setText("По вашему запросу ничего не найдено. Попробуйте изменить данные поиска.");
					}});
				}
				U.finishLoader();
			}
		});
	}

	private LinearLayout newItem(final MyUser zoo) {
		LinearLayout item = (LinearLayout) getLayoutInflater().inflate(R.layout.performer_item, performer_items_layout, false);

		item.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				gotoUserProfile(zoo);
			}
		});

		final ImageView avatar = item.findViewById(R.id.avatar);
		UserService.getRoundedAvatar(this, zoo.getUuid(), new OnDataCallback<Bitmap>(this) {
			@Override
			public void action(Bitmap bitmap) {
				if (bitmap == null) return;
				avatar.setImageBitmap(bitmap);
			}
		});

		TextView price = item.findViewById(R.id.zoo_price);
		price.setText(Integer.toString(zoo.getPrice()));

		TextView zooName = item.findViewById(R.id.zoo_name);
		zooName.setText(zoo.getName());

		TextView number_of_completed_tasks = item.findViewById(R.id.number_of_completed_tasks);
		number_of_completed_tasks.setText("" + zoo.booksPerformed());

		TextView rating_number = item.findViewById(R.id.rating_number);
		rating_number.setText("" + zoo.rating());

		RatingBar rating = item.findViewById(R.id.zoo_rating);
		rating.setRating((float) zoo.rating());

		TextView zooAddress = item.findViewById(R.id.zoo_address);
		zooAddress.setText(zoo.getFullAddress());

		AnimalType animalType = zoo.getAnimalType();
		if (animalType == AnimalType.Cat || animalType == AnimalType.All) {
			ImageView catView = item.findViewById(R.id.cat_img);
			catView.setVisibility(View.VISIBLE);
		}
		if (animalType == AnimalType.Dog || animalType == AnimalType.All) {
			ImageView catView = item.findViewById(R.id.dog_img);
			catView.setVisibility(View.VISIBLE);
		}

		final ImageView blockImageView = item.findViewById(R.id.blockImageView);
		List<String> activeBookingCustomerUuids = zoo.activeBookingCustomerUuids();
		if (!zoo.getZooIsLocked() && !activeBookingCustomerUuids.contains(UserService.currentUser.getUuid())) {
			blockImageView.setVisibility(View.INVISIBLE);
		} else {
			if (activeBookingCustomerUuids.contains(UserService.currentUser.getUuid())) {
				blockImageView.setImageResource(R.drawable.mark_256);
			} else {
				blockImageView.setImageResource(R.drawable.block_256);
			}
		}

		return item;
	}

	private void gotoUserProfile(MyUser user) {
		Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("user", user);
		if (getIntent().hasExtra(EXTRA_NAME)) {
			Timber.d("putSerializable extra %s", getIntent().getStringExtra(EXTRA_NAME));
			b.putSerializable("name", getIntent().getStringExtra(EXTRA_NAME));
		}
		intent.putExtras(b);
		startActivity(intent);
	}

	private boolean checkPlayServices() {
		GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
		int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (apiAvailability.isUserResolvableError(resultCode)) {
				apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
					.show();
			} else {
				Timber.i("This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}
}
