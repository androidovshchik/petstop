package petstop.overcode.ru.petstop.service;

/**
 * Created by VIP on 25.08.2017.
 */

public abstract class GetDataFunc<TOut, TIn> {
	public abstract TOut call(TIn param);
}
