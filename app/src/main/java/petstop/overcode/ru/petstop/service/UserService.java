package petstop.overcode.ru.petstop.service;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import petstop.overcode.ru.petstop.BaseActivity;
import petstop.overcode.ru.petstop.R;
import petstop.overcode.ru.petstop.dto.AnimalType;
import petstop.overcode.ru.petstop.dto.FilterDto;
import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.dto.P2PDebitResult;
import petstop.overcode.ru.petstop.dto.ZooBooking;
import petstop.overcode.ru.petstop.util.AuthUtil;
import petstop.overcode.ru.petstop.util.ImageHelper;

/**
 * Created by VIP on 25.08.2017.
 */

public class UserService {
	public static volatile MyUser currentUser;
	private static HashMap<String, Bitmap> imageCache = new HashMap<>();
	private static Bitmap defaultAvatar;

	public static void getZoo(final FilterDto filter, final OnDataCallback<List<MyUser>> callback) {
		mkQuery(Fb.ref().child("users").limitToFirst(100), filter).addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				List<MyUser> users = new ArrayList<>();
				for (DataSnapshot userData : dataSnapshot.getChildren()) {
					MyUser user = userData.getValue(MyUser.class);
					users.add(user);
				}
				callback.doAction(filter(users, filter));
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {
			}
		});
	}

	private static Query mkQuery(Query ref, FilterDto filter) {
		if (filter.getCity().isEmpty()) return ref;
		Query query = ref
			.orderByChild("city")
			.equalTo(filter.getCity());
		return query;
	}

	private static List<MyUser> filter(List<MyUser> users, FilterDto filter) {
		List<MyUser> filteredUsers = new ArrayList<>();
		for (MyUser user : users) {
			if (user.getIsZoo()
				&& (filter.getAnimalType() == AnimalType.All || user.getAnimalType() == filter.getAnimalType() || user.getAnimalType() == AnimalType.All)
				&& user.getPrice() >= filter.getPriceMin()
				&& user.getPrice() <= filter.getPriceMax()
				&& user.rating() >= filter.getMinRating()
				&& user.getMinDays() >= filter.getMinDays()
				&& (user.getCity().equals(filter.getCity()) || filter.getCity().isEmpty())
				&& (user.getMetro().equals(filter.getMetro()) || filter.getMetro().isEmpty())
				&& (user.getStreet().equals(filter.getStreet()) || filter.getStreet().isEmpty())
				) {
				filteredUsers.add(user);
			}
		}

		Collections.sort(filteredUsers, new Comparator<MyUser>() {
			@Override
			public int compare(MyUser u1, MyUser u2) {
				return -Long.valueOf(u1.getTop1PaymentDate()).compareTo(u2.getTop1PaymentDate());
			}
		});

		return filteredUsers;
	}

	private static StorageReference getAvatarStorage() {
		return FirebaseStorage.getInstance().getReference().child("avatars");
	}

	public static void uploadAvatar(Bitmap bitmap) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] imageInByte = baos.toByteArray();

		imageCache.put(AuthUtil.getUser().getUid(), bitmap);
		getAvatarStorage().child(AuthUtil.getUser().getUid()).putBytes(imageInByte);
	}

	public static void getRoundedAvatar(final Activity activity, final String uuid, final OnDataCallback<Bitmap> callback) {
		getAvatar(uuid, new OnDataCallback<Bitmap>(callback.activity) {
			@Override
			public void action(Bitmap data) {
				callback.doAction(ImageHelper.getRoundedCornerBitmap(activity, data));
			}
		});
	}

	public static void getRectAvatar(final Activity activity, final String uuid, final OnDataCallback<Bitmap> callback) {
		getAvatar(uuid, new OnDataCallback<Bitmap>(callback.activity) {
			@Override
			public void action(Bitmap data) {
				callback.doAction(ImageHelper.getRectBitmap(activity, data));
			}
		});
	}

	private static void getAvatar(final String uuid, final OnDataCallback<Bitmap> callback) {
		if (imageCache.containsKey(uuid)) {
			callback.doAction(imageCache.get(uuid));
			return;
		}

		getAvatarStorage().child(uuid).getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
			@Override
			public void onSuccess(byte[] bytes) {
				Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
				imageCache.put(uuid, bitmap);
				callback.doAction(bitmap);
			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				if (defaultAvatar == null) {
					defaultAvatar = BitmapFactory.decodeResource(
						BaseActivity.publicContext.getResources(), R.drawable.default_avatar);
				}
				imageCache.put(uuid, defaultAvatar);
				callback.doAction(defaultAvatar);
			}
		});
	}

	public static void updateUserData(MyUser user) {
		Fb.ref().child("users").child(user.getUuid()).setValue(user);
	}

	public static void setCardToken(String userUuid, String token, final OnDataCallback<Void> callback) {
		Fb.ref().child("users").child(userUuid).child("cardToken").setValue(token)
			.addOnCompleteListener(new OnCompleteListener<Void>() {
				@Override
				public void onComplete(@NonNull Task<Void> task) {
					callback.doAction(null);
				}
			});
	}

	public static void setBooked(MyUser zoo, P2PDebitResult result, final OnDataCallback<Void> callback) {
		ZooBooking booking = new ZooBooking();
		booking.setOrder(result);
		booking.setIsActive(true);
		booking.setCustomerId(currentUser.getUuid());
		booking.setDate(new Date());
		booking.setRating(-1);
		booking.setNumber(zoo.getBookings().size());
		zoo.getBookings().add(booking);
		zoo.setZooIsLocked(false);
		final DatabaseReference userRef = Fb.ref().child("/users").child(zoo.getUuid());
		userRef.child("bookings").child(booking.getNumber() + "").setValue(booking)
			.addOnCompleteListener(new OnCompleteListener<Void>() {
				@Override
				public void onComplete(@NonNull Task<Void> task) {
					userRef.child("zooIsLocked").setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
						@Override
						public void onComplete(@NonNull Task<Void> task) {
							callback.doAction(null);
						}
					});
				}
			});
	}

	public static void setLockCurrentZoo(boolean val, final OnDataCallback<Void> callback) {
		UserService.currentUser.setZooIsLocked(val);
		Fb.ref().child("/users").child(AuthUtil.getUser().getUid()).child("zooIsLocked").setValue(val)
			.addOnCompleteListener(new OnCompleteListener<Void>() {
				@Override
				public void onComplete(@NonNull Task<Void> task) {
					callback.doAction(null);
				}
			});
	}

	public static void setOrderCompletedOnCurrentZoo(ZooBooking booking, final OnDataCallback<Void> callback) {
		Fb.ref().child("/users").child(AuthUtil.getUser().getUid()).child("bookings").child(booking.getNumber() + "").child("order").child("isDone").setValue(true)
			.addOnCompleteListener(new OnCompleteListener<Void>() {
				@Override
				public void onComplete(@NonNull Task<Void> task) {
					callback.doAction(null);
				}
			});
	}

	public static void completeBooking(final MyUser zoo, final OnDataCallback<Integer> callback) {
		final ZooBooking currentBooking = zoo.getActiveBookingForUser(AuthUtil.getUser().getUid());

		if (currentBooking == null) return;

		AsyncTask.execute(new Runnable() {
			@Override
			public void run() {
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					@Override
					public void run() {
						final Integer bookingId = currentBooking.getNumber();
						currentBooking.setIsActive(false);
						final DatabaseReference zooRef = Fb.ref().child("/users").child(zoo.getUuid());
						zooRef.child("bookings").child(currentBooking.getNumber() + "").child("isActive").setValue(false)
							.addOnCompleteListener(new OnCompleteListener<Void>() {
								@Override
								public void onComplete(@NonNull Task<Void> task) {
									zooRef.child("zooIsLocked").setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
										@Override
										public void onComplete(@NonNull Task<Void> task) {
											zoo.setZooIsLocked(false);
											callback.doAction(bookingId);
										}
									});
								}
							});
					}
				});
			}
		});
	}

	public static void completeBookPayment(final String zooUuid, final String bookingId, final OnDataCallback<Void> callback) {
		final DatabaseReference bookingRef = Fb.ref().child("/users").child(zooUuid).child("bookings").child(bookingId);
		bookingRef.child("order").child("isDone").setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				bookingRef.child("isActive").setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
					@Override
					public void onComplete(@NonNull Task<Void> task) {
						callback.doAction(null);
					}
				});
			}
		});
	}

	public static void getCurrentUser(final OnDataCallback<MyUser> call) {
		Fb.withTimeout(new Runnable() {
			@Override
			public void run() {
				if (currentUser != null) {
					call.doAction(currentUser);
					return;
				}

				if (FirebaseAuth.getInstance().getCurrentUser() == null) {
					call.doAction(null);
					return;
				}

				Fb.ref().child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
					.addValueEventListener(new ValueEventListener() {
						@Override
						public void onDataChange(DataSnapshot dataSnapshot) {
							MyUser newUser = dataSnapshot.getValue(MyUser.class);
							if (newUser == null) {
								AuthUtil.getAuth().signOut();
								UserService.currentUser = null;
								throw new RuntimeException("user was deleted");
							}
							newUser.setUuid(FirebaseAuth.getInstance().getCurrentUser().getUid());

							// if(currentUser != null && (!isEqual(currentUser.getCardToken(), newUser.getCardToken())
							//         || !isEqual(currentUser.getTop1PaymentDate(), newUser.getTop1PaymentDate()))){
							//     currentUser = newUser;
							// }else
							{
								currentUser = newUser;
								call.doAction(currentUser);
							}
						}

						@Override
						public void onCancelled(DatabaseError databaseError) {
						}
					});
			}
		}, call);
	}

	private static boolean isEqual(Object o1, Object o2) {
		return o1 == null && o2 == null || o1 != null && o1.equals(o2);
	}

	public static void getTransferCode(String zooUuid, String orderId, final OnDataCallback<String> call) {
		Fb.ref().child("transfer_code").child(zooUuid + orderId)
			.addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(DataSnapshot dataSnapshot) {
					call.doAction((String) dataSnapshot.getValue());
				}

				@Override
				public void onCancelled(DatabaseError databaseError) {
					call.doFail(databaseError.getMessage());
				}
			});
	}

	public static void setTopForUser(MyUser user, final OnDataCallback<Void> callback) {
		user.setTop1PaymentDate(new Date().getTime());
		Fb.ref().child("users").child(user.getUuid()).child("top1PaymentDate")
			.setValue(user.getTop1PaymentDate()).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				callback.doAction(null);
			}
		});
	}
}
