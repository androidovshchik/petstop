package petstop.overcode.ru.petstop.service;

import android.app.Activity;
import android.content.Intent;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Timer;
import java.util.TimerTask;

import petstop.overcode.ru.petstop.ErrorActivity;
import petstop.overcode.ru.petstop.util.U;

/**
 * Created by VIP on 25.08.2017.
 */

public class Fb {
	public static DatabaseReference ref() {
		return FirebaseDatabase.getInstance().getReference();
	}

	public static void withTimeout(final Runnable runnable, final OnDataCallback callback) {
		final Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (!callback.isDone) {
					error(callback.activity);
					timer.cancel();
				} else {
					timer.cancel();
				}
			}
		}, 10 * 1000, Long.MAX_VALUE);
		try {
			runnable.run();
		} catch (Exception e) {
			error(callback.activity);
		}
	}

	private static void error(Activity activity) {
		U.finishLoader();
		Intent startApp = new Intent(activity.getApplicationContext(), ErrorActivity.class);
		startApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		activity.startActivity(startApp);
		activity.finish();
	}
}
