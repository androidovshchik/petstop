package petstop.overcode.ru.petstop.service;

import android.app.Activity;

/**
 * Created by VIP on 25.08.2017.
 */

public abstract class OnDataCallback<T> {

	public Activity activity;
	public boolean isDone = false;

	public OnDataCallback(Activity activity) {
		this.activity = activity;
	}

	public abstract void action(T data);

	public void doAction(final T data) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				isDone = true;
				action(data);
			}
		});
	}

	public void fail(String msg) {
	}

	public void doFail(final String msg) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				isDone = true;
				fail(msg);
			}
		});
	}
}
