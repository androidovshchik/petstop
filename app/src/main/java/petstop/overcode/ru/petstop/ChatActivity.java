package petstop.overcode.ru.petstop;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.github.huajianjiang.expandablerecyclerview.widget.ExpandableAdapter;
import com.github.huajianjiang.expandablerecyclerview.widget.ParentViewHolder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import petstop.overcode.ru.petstop.adapters.ChatAdapter;
import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.dto.Room;
import petstop.overcode.ru.petstop.service.ChatService;
import petstop.overcode.ru.petstop.service.Fb;
import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.service.UserService;
import timber.log.Timber;

public class ChatActivity extends AppCompatActivity {

	@Nullable
	public MyUser zoo;
	private ChatAdapter chatAdapter;
	private int expandPosition = -1;
	private ValueEventListener chatListener;
	private DatabaseReference reference;
	private RecyclerView recyclerView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat);

		if (getIntent().hasExtra("user")) {
			zoo = (MyUser) getIntent().getExtras().getSerializable("user");
		}

		findViewById(R.id.goToBackBtnComment).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
		recyclerView = findViewById(R.id.chat);
		recyclerView.setLayoutManager(layoutManager);
		if (UserService.currentUser.getIsZoo()) {
			chatAdapter = new ChatAdapter(getApplicationContext(), UserService.currentUser.getUuid(),
				UserService.currentUser.getName(), null, null, null, false);
		} else {
			chatAdapter = new ChatAdapter(getApplicationContext(), null, null,
				UserService.currentUser.getUuid(), UserService.currentUser.getName(), null, true);
		}
		chatAdapter.setExpandCollapseMode(ExpandableAdapter.ExpandCollapseMode.MODE_SINGLE_EXPAND);
		chatAdapter.addParentExpandCollapseListener(new ExpandableAdapter.OnParentExpandCollapseListener() {
			@Override
			public void onParentExpanded(RecyclerView rv, final ParentViewHolder pvh, int position,
										 boolean pendingCause, boolean byUser) {
				if (pvh == null) return;
				Timber.tag(Constants.TAG_CHAT).d("One parent expanded");
				ImageView arrow = pvh.getView(R.id.arrow);
				arrow.setRotation(90);
				expandPosition = position;
				markRoom((Room) pvh.getParent());
			}

			@Override
			public void onParentCollapsed(RecyclerView rv, ParentViewHolder pvh, int position,
										  boolean pendingCause, boolean byUser) {
				if (pvh == null) return;
				ImageView arrow = pvh.getView(R.id.arrow);
				arrow.setRotation(0);
				expandPosition = -1;
			}
		});
		recyclerView.setAdapter(chatAdapter);
		ChatService.counters.clear();
		reference = Fb.ref().child("chat");
	}

	private Room getRoomForZoo() {
		Room newRoom = new Room();
		newRoom.customerUuid = UserService.currentUser.getUuid();
		newRoom.customerName = UserService.currentUser.getName();
		newRoom.zooUuid = zoo.getUuid();
		newRoom.zooName = zoo.getName();
		newRoom.counter = 0;
		newRoom.setup(true);
		return newRoom;
	}

	@Override
	protected void onStart() {
		super.onStart();
		chatListener = reference.addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(DataSnapshot dataSnapshot) {
					ChatService.onRooms(dataSnapshot, !UserService.currentUser.getIsZoo(),
						UserService.currentUser.getUuid(), new OnDataCallback<ArrayList<Room>>(null) {
							@Override
							public void action(final ArrayList<Room> rooms) {
								Timber.tag(Constants.TAG_CHAT).d("Get rooms task: callback");
								if (zoo != null) {
									if (rooms.size() == 0) {
										rooms.add(getRoomForZoo());
										expandPosition = 0;
									} else {
										for (int i = 0; i < rooms.size(); i++) {
											if (rooms.get(i).zooUuid.equals(zoo.getUuid())) {
												expandPosition = i;
												break;
											}
											if (i == rooms.size() - 1) {
												rooms.add(getRoomForZoo());
												expandPosition = i + 1;
												break;
											}
										}
									}
								}
								chatAdapter.invalidate(rooms);
								if (expandPosition != -1 &&
									chatAdapter.getParents().size() > expandPosition) {
									chatAdapter.expandParent(expandPosition);
								}
								recyclerView.postInvalidate();
							}
						});
				}

				@Override
				public void onCancelled(DatabaseError databaseError) {}
			});
	}

	private void markRoom(final Room room) {
		final boolean isCustomer = UserService.currentUser.isCustomer();
		boolean hasChanges = false;
		for (int i = 0; i < room.messages.size(); i++) {
			if (room.hasUnreadForMe(isCustomer, i)) {
				Timber.tag(Constants.TAG_CHAT).d("Mark task: mark function has changes");
				hasChanges = true;
				break;
			}
		}
		if (hasChanges) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Timber.tag(Constants.TAG_CHAT).d("Mark task: run after delay");
					ChatService.markAsRead(room, !isCustomer);
				}
			}, 1000);
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		reference.removeEventListener(chatListener);
	}

	@Override
	@SuppressWarnings("ConstantConditions")
	public boolean dispatchTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			View view = getCurrentFocus();
			if (view != null && view instanceof EditText) {
				view.clearFocus();
				InputMethodManager inputMethodManager = (InputMethodManager)
					getSystemService(Context.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
			}
		}
		return super.dispatchTouchEvent(event);
	}

	@Override
	public void onBackPressed() {}
}
