package petstop.overcode.ru.petstop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class RegisterAsActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_as);

		Button signupAsZooBtn = (Button) findViewById(R.id.signup_as_zoo_btn);
		signupAsZooBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), RegisterAsZoo.class);
				startActivity(intent);
			}
		});

		Button signupAsCustomerBtn = (Button) findViewById(R.id.signup_as_customer_btn);
		signupAsCustomerBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), RegisterAsCustomer.class);
				startActivity(intent);
			}
		});
	}
}
