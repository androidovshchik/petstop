package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import petstop.overcode.ru.petstop.service.OnDataCallback;

public class ApplyDialog extends Dialog {

	public Activity c;
	public Dialog d;
	private OnDataCallback<Void> yesCallback;
	private OnDataCallback<Void> noCallback;
	public ApplyDialog(Activity context, final OnDataCallback<Void> yesCallback, final OnDataCallback<Void> noCallback) {
		super(context);

		this.c = context;
		this.yesCallback = yesCallback;
		this.noCallback = noCallback;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_apply_dialog);


		findViewById(R.id.yesButtonBooking).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				yesCallback.doAction(null);
			}
		});
		findViewById(R.id.noButtonBooking).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				noCallback.doAction(null);
			}
		});
	}
}
