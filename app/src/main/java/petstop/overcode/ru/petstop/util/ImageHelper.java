package petstop.overcode.ru.petstop.util;

/**
 * Created by VIP on 04.09.2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.view.Display;

public class ImageHelper {
	public static Bitmap getRoundedCornerBitmap(Activity activity, Bitmap bitmap) {
		final int color = 0xff424242;
		final Paint paint = new Paint();
		int w = bitmap.getWidth(), h = bitmap.getHeight();
		int d = Math.min(w, h);
		final Rect rect = new Rect((w - d) / 2, (h - d) / 2, d + (w - d) / 2, d + (h - d) / 2);
		final float roundPx = d / 2;

		Bitmap output = Bitmap.createBitmap(d, d, Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(new RectF(0, 0, d, d), roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, new Rect(0, 0, d, d), paint);

		return output;
	}

	public static Bitmap getRectBitmap(Activity activity, Bitmap bitmap) {
		Display display = activity.getWindowManager().getDefaultDisplay();
		int dw = display.getWidth(), dh = convertDpToPixel(230, activity.getApplicationContext());
		int bw = bitmap.getWidth(), bh = bitmap.getHeight();
		double dk = ((double) dw) / dh, bk = ((double) bw) / bh;

		int diffW = 0, diffH = 0;
		if (dk > bk) {
			int newBh = (int) (bw / dk);
			diffH = (bh - newBh) / 2;
			bh = newBh;
		} else {
			int newBw = (int) (bh * dk);
			diffW = (bw - newBw) / 2;
			bw = newBw;
		}

		Bitmap output = Bitmap.createBitmap(dw, dh, Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(diffW, diffH, bw + diffW, bh + diffH);

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 57, 0);
		paint.setColor(color);
		canvas.drawBitmap(bitmap, rect,
			new Rect(0, 0, output.getWidth(), output.getHeight()), paint);
		return output;
	}

	public static int convertDpToPixel(int dp, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		return (int) (dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
	}
}