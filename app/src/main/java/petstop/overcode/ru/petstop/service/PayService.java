package petstop.overcode.ru.petstop.service;

import android.os.AsyncTask;
import android.os.StrictMode;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.IOException;
import java.util.ArrayList;

import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.dto.P2PDebitResult;
import petstop.overcode.ru.petstop.dto.ZooBooking;
import petstop.overcode.ru.petstop.util.U;
import petstop.overcode.ru.petstop.util.XmlUtil;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import timber.log.Timber;

/**
 * Created by VIP on 20.09.2017.
 */

public class PayService {
	public static final String url = "http://ireneb5x.beget.tech/";
	final static int GetCardTokenAmount = 1000;
	final static String adminMail = "info@pets-app.ru";
	private static IPayService service = new Retrofit.Builder()
		.baseUrl(url)
		.addConverterFactory(ScalarsConverterFactory.create())
		.build()
		.create(IPayService.class);
	private static int GET_TOKEN = 1;
	private static int currency = 643;
	private static String payForTopDescription = "Оплата Топ 1";
	private static String checkCardDescription = "Проверка карты";
	private static String onmsgurl = url + "/my.php";

	public static String PayForTop(int amount, String email, String userUuid, String token) {
		setThreadPolicy();

		try {
			String orderId = service.PurchaseRegister(amount, currency, email, payForTopDescription, 1, userUuid, onmsgurl)
				.execute().body();

			String url = service.Purchase(orderId, token, GET_TOKEN)
				.request().url().toString();
			return url;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String PayForBooking(String customerUuid, int amount, String email, String token) {
		setThreadPolicy();

		try {

			final String orderId = service.Register(amount, currency, email,
				"Оплата за бронирование няни", 1,
				customerUuid, onmsgurl)
				.execute().body();

			return service.P2PDebit(orderId, token, GET_TOKEN).request().url().toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void PushInterest(MyUser user, ArrayList<String> tokens, String name) {
		setThreadPolicy();

		try {
			String response = service.PushInterest(new JSONArray(tokens).toString(), name)
				.execute().body();
			Timber.d(response);
			JSONObject jsonObject = new JSONObject(response);
			FCMTokenService.removeTokens(user.getUuid(), jsonObject.getJSONArray("results"));
		} catch (Exception e) {
			Timber.e(e);
		}
	}

	public static void PushMessage(String uuid, ArrayList<String> tokens, String name) {
		setThreadPolicy();

		try {
			String response = service.PushMessage(uuid, name, new JSONArray(tokens).toString())
				.execute().body();
			Timber.d(response);
			JSONObject jsonObject = new JSONObject(response);
			FCMTokenService.removeTokens(uuid, jsonObject.getJSONArray("results"));
		} catch (Exception e) {
			Timber.e(e);
		}
	}

	public static String GetCardToken_AuthorizeUrl() {
		setThreadPolicy();

		final String orderId;
		try {
			orderId = service.Register(GetCardTokenAmount, currency, UserService.currentUser.getEmail(), checkCardDescription, 1,
				"Проверка карты", onmsgurl).execute().body();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return service.Authorize(Integer.valueOf(orderId), "1")
			.request().url().toString();
	}

	public static String GetStatus(String orderId, String operationId) {
		String operationStatusXml = null;
		try {
			operationStatusXml = service.Operation(orderId, operationId, 0).execute().body();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		Document document = XmlUtil.parse(operationStatusXml);
		String state = XmlUtil.get(document, "state");
		if (!"APPROVED".equals(state)) {
			return XmlUtil.get(document, "message");
		}
		return state;
	}

	public static String GetPurchaseStatus(String orderId, String operationId) {
		String operationStatusXml = null;
		try {
			operationStatusXml = service.PurchaseOperation(orderId, operationId, 0).execute().body();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		Document document = XmlUtil.parse(operationStatusXml);
		String state = XmlUtil.get(document, "state");
		if (!"APPROVED".equals(state)) {
			return XmlUtil.get(document, "message");
		}
		return state;
	}

	public static String GetCardToken_Token(String onmsgUrl) {
		setThreadPolicy();

		String orderId = U.getParam(onmsgUrl, "id");
		String operationId = U.getParam(onmsgUrl, "operation");
		if (operationId == null || operationId.isEmpty() || "0".equals(operationId)) {
			throw new RuntimeException("Ошибка сервиса оплаты");
		}

		String operationStatusXml = null;
		try {
			operationStatusXml = service.Operation(orderId, operationId, GET_TOKEN).execute().body();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		Document document = XmlUtil.parse(operationStatusXml);
		String state = XmlUtil.get(document, "state");
		if (!"APPROVED".equals(state)) {
			throw new RuntimeException("Operation state: " + state + ", reason_code = " + XmlUtil.get(document, "reason_code"));
		}
		String token = XmlUtil.get(document, "token");

		String reverseStatusXml = null;
		try {
			reverseStatusXml = service.Reverse(orderId, GetCardTokenAmount, currency).execute().body();

			Document reverseDocument = XmlUtil.parse(reverseStatusXml);
			String reverseState = XmlUtil.get(reverseDocument, "state");
			System.out.println("Operation state: " + reverseState + ", reason_code = " + XmlUtil.get(reverseDocument, "reason_code"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return token;
	}

	public static void completeP2PDebit(final String zooUuid, final String zooCardToken, final ZooBooking booking, final OnDataCallback<Void> callback) {
		setThreadPolicy();
		try {
			final P2PDebitResult order = booking.getOrder();
			UserService.getTransferCode(booking.getCustomerId(), order.getOrderId(), new OnDataCallback<String>(callback.activity) {
				@Override
				public void action(final String transferCode) {
					if (transferCode == null) return;
					AsyncTask.execute(new Runnable() {
						@Override
						public void run() {
							String completeUrl = service.P2PComplete(order.getOrderId(), zooCardToken, transferCode, "pay").request().url().toString();
							String response = U.fetchResponseUrl(completeUrl);
							if (response.contains("ОДОБРЕНА") || response.contains("успешно")) {
								UserService.completeBookPayment(zooUuid, booking.getNumber() + "", callback);
							}
						}
					});
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private static void setThreadPolicy() {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
	}

	public static void sendMail(String from, String to, String subject, String message) {
		U.fetchResponseUrl(url + "SendMail.php?" + "from=" + from + "&to=" + to + "&subject=" + subject + "&message=" + message);
	}

	public static void sendMailToAdmin(String from, String subject, String message) {
		sendMail(from, adminMail, subject, message);
	}

	public static void sendFromAdmin(String to, String subject, String message) {
		sendMail(adminMail, to, subject, message);
	}
}
