package petstop.overcode.ru.petstop.service;

import android.app.IntentService;
import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceId;

public class RegistrationIntentService extends IntentService {

	private static final String TAG = "RegistrationIntentService";

	public RegistrationIntentService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		FirebaseInstanceId instanceID = FirebaseInstanceId.getInstance();
		String token = instanceID.getToken();
		if (UserService.currentUser != null) {
			FCMTokenService.addToken(UserService.currentUser, token);
		}
	}
}