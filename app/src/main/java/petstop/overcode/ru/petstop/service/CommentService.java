package petstop.overcode.ru.petstop.service;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import petstop.overcode.ru.petstop.dto.Comment;

/**
 * Created by VIP on 27.08.2017.
 */

public class CommentService {
	public static void add(final Comment comment) {
		Fb.ref().child("comments").child(comment.getUserUuid()).child("" + comment.getBookingId()).setValue(comment);
		if (comment.getRating() > 0) {
			Fb.ref().child("users").child(comment.getUserUuid()).child("bookings").child("" + comment.getBookingId()).child("rating").setValue(comment.getRating());
		}
	}

	public static void forUser(String userUuid, final OnDataCallback<List<Comment>> callback) {
		Fb.ref().child("comments").child(userUuid).addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				List<Comment> comments = new ArrayList<>();
				for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
					Comment comment = snapshot.getValue(Comment.class);
					comments.add(comment);
				}
				callback.doAction(comments);
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {
			}
		});
	}
}
