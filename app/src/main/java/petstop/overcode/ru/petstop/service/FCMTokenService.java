package petstop.overcode.ru.petstop.service;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import petstop.overcode.ru.petstop.dto.MyUser;
import timber.log.Timber;

public class FCMTokenService {

	private static final GenericTypeIndicator<ArrayList<String>> TYPE_INDICATOR =
		new GenericTypeIndicator<ArrayList<String>>() {
		};

	public static void addToken(final MyUser user, final String token) {
		Fb.ref().child("fcmTokens").child(user.getUuid()).addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				ArrayList<String> tokens = dataSnapshot.getValue(TYPE_INDICATOR);
				if (tokens != null) {
					tokens.removeAll(Collections.<String>singleton(null));
					for (int i = 0; i < tokens.size(); i++) {
						if (tokens.get(i).equals(token)) {
							break;
						}
						if (i == tokens.size() - 1) {
							tokens.add(token);
						}
					}
				} else {
					tokens = new ArrayList<>();
					tokens.add(token);
				}
				Fb.ref().child("fcmTokens").child(user.getUuid())
					.setValue(tokens);
			}

			@Override
			public void onCancelled(DatabaseError firebaseError) {
			}
		});
	}

	public static void getTokens(String userUuid, final OnDataCallback<ArrayList<String>> callback) {
		Fb.ref().child("fcmTokens").child(userUuid).addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				ArrayList<String> tokens = dataSnapshot.getValue(TYPE_INDICATOR);
				if (tokens != null) {
					tokens.removeAll(Collections.<String>singleton(null));
				} else {
					tokens = new ArrayList<>();
				}
				callback.isDone = true;
				callback.action(tokens);
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {
			}
		});
	}

	public static void removeToken(final MyUser user, final String token, final OnDataCallback<Void> callback) {
		Fb.ref().child("fcmTokens").child(user.getUuid()).addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				ArrayList<String> tokens = dataSnapshot.getValue(TYPE_INDICATOR);
				if (tokens != null) {
					tokens.removeAll(Collections.<String>singleton(null));
					for (int i = 0; i < tokens.size(); i++) {
						if (tokens.get(i).equals(token)) {
							tokens.remove(i);
							break;
						}
					}
				} else {
					tokens = new ArrayList<>();
				}
				Fb.ref().child("fcmTokens").child(user.getUuid())
					.setValue(tokens);
				callback.isDone = true;
				callback.action(null);
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {
			}
		});
	}

	public static void removeTokens(final String userUuid, final JSONArray jsonTokens) {
		Fb.ref().child("fcmTokens").child(userUuid).addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				ArrayList<String> tokens = dataSnapshot.getValue(TYPE_INDICATOR);
				if (tokens != null) {
					tokens.removeAll(Collections.<String>singleton(null));
					if (jsonTokens.length() != tokens.size()) {
						return;
					}
					try {
						for (int i = tokens.size() - 1; i >= 0; i--) {
							JSONObject object = jsonTokens.getJSONObject(i);
							if (object.has("registration_id") || object.has("error")) {
								tokens.remove(i);
							}
						}
					} catch (JSONException e) {
						Timber.e(e);
					}
				} else {
					tokens = new ArrayList<>();
				}
				Fb.ref().child("fcmTokens").child(userUuid)
					.setValue(tokens);
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {
			}
		});
	}
}
