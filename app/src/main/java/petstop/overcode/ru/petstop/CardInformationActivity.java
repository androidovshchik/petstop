package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.service.PayService;
import petstop.overcode.ru.petstop.service.UserService;
import petstop.overcode.ru.petstop.util.U;

public class CardInformationActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_card_information);

		final WebView webView = U.initWebView(this, new WebView(this), GetOnTokenCallback());

		findViewById(R.id.goToBackBtnCardInfo).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		final Activity thisActivity = this;

		final Button button = (Button) findViewById(R.id.saveCardInfoButton);
		if (UserService.currentUser.getCardToken() != null) {
			button.setVisibility(View.VISIBLE);
			button.setText("Обновить карту");
		} else {
			button.setVisibility(View.GONE);
			runWebView(thisActivity, webView);
		}

		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				U.rm(button);
				try {
					runWebView(thisActivity, webView);
				} catch (Exception e) {
					U.finishLoader();
					runOnUiThread(new Runnable() {
						public void run() {
							U.longToast(thisActivity, "Ошибка сервиса оплаты");
						}
					});
				}
			}
		});

		ScrollView scroll = (ScrollView) findViewById(R.id.scrollInCard);
		scroll.fullScroll(ScrollView.FOCUS_UP);

		LinearLayout ll = (LinearLayout) findViewById(R.id.layoutInCard);
		ll.addView(webView);
	}

	private void runWebView(Activity thisActivity, final WebView webView) {
		U.startLoader(thisActivity, "Загрузка формы регистрации");

		AsyncTask.execute(new Runnable() {
			@Override
			public void run() {
				final String authorizeUrl = PayService.GetCardToken_AuthorizeUrl();
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						webView.loadUrl(authorizeUrl);
					}
				});
			}
		});
	}


	OnDataCallback<String> GetOnTokenCallback() {
		final Activity thisActivity = this;
		return new OnDataCallback<String>(this) {
			@Override
			public void action(final String onmsgUrl) {
				if (onmsgUrl.contains("my.php")) {
					AsyncTask.execute(new Runnable() {
						@Override
						public void run() {
							try {
								final String status = PayService.GetStatus(U.getParam(onmsgUrl, "id"), U.getParam(onmsgUrl, "operation"));
								if (!"APPROVED".equals(status)) {
									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											U.longToast(thisActivity, "Ошибка проверки. " + status);
										}
									});
									try {
										Thread.sleep(3500);
									} catch (InterruptedException e) {
									}
									thisActivity.finish();
								} else {
									final String token = PayService.GetCardToken_Token(onmsgUrl);

									runOnUiThread(new Runnable() {
										public void run() {
											final MyUser user = UserService.currentUser;
											user.setCardToken(token);
											UserService.setCardToken(user.getUuid(), token, new OnDataCallback<Void>(thisActivity) {
												@Override
												public void action(Void data) {
													Toast.makeText(thisActivity, "Сохранено", Toast.LENGTH_SHORT).show();
													finish();
												}
											});
										}
									});
								}
							} catch (Exception e) {
								e.printStackTrace();
								fail("Ошибка проверки");
							}
						}
					});
				} else {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							U.finishLoader();
						}
					});
				}
			}
		};
	}


}
