package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.webkit.JavascriptInterface;

import petstop.overcode.ru.petstop.service.OnDataCallback;

/**
 * Created by jomoj on 23.09.2017.
 */

public class JavaScriptInterface {
	private Activity activity;
	private OnDataCallback<String> onTokenCallback;

	public JavaScriptInterface(Activity activiy, OnDataCallback<String> onTokenCallback) {
		this.activity = activiy;
		this.onTokenCallback = onTokenCallback;
	}

	@JavascriptInterface
	public void doSomething(String str) {
		onTokenCallback.doAction(str);
	}
}
