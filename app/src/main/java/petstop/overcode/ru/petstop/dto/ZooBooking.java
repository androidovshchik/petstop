package petstop.overcode.ru.petstop.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by VIP on 14.09.2017.
 */

public class ZooBooking implements Serializable {

	private int number;
	private boolean isActive;
	private String customerId;
	private int rating;
	private Date date;
	private P2PDebitResult order;

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean active) {
		isActive = active;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public P2PDebitResult getOrder() {
		return order;
	}

	public void setOrder(P2PDebitResult order) {
		this.order = order;
	}
}
