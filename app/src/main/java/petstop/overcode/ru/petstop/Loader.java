package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class Loader extends Dialog implements
	android.view.View.OnClickListener {

	public Activity activity;
	public Dialog d;
	private String message;

	public Loader() {
		super(null);

	}

	public Loader(Activity activity, String message) {
		super(activity);
		this.activity = activity;
		this.message = (message == null ? "Загрузка" : message) + " ...";
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setCancelable(false);
		setContentView(R.layout.activity_loader);
		((TextView) findViewById(R.id.loaderText)).setText(message);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	}

	@Override
	public void onClick(View view) {
	}
}