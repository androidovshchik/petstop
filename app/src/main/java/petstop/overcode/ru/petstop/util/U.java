package petstop.overcode.ru.petstop.util;

import android.app.Activity;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import petstop.overcode.ru.petstop.JavaScriptInterface;
import petstop.overcode.ru.petstop.Loader;
import petstop.overcode.ru.petstop.R;
import petstop.overcode.ru.petstop.service.OnDataCallback;

/**
 * Created by VIP on 30.08.2017.
 */

public class U {
	private static Loader loader;

	public static void longToast(Activity activity, String msg) {
		Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
	}

	public static boolean isEmailValid(String email) {
		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public static void rm(View view) {
		if (view == null) return;
		((ViewGroup) view.getParent()).removeView(view);
	}

	public static void startLoader(Activity activity, String message) {
		if (activity.isFinishing()) return;
		loader = new Loader(activity, message);
		loader.show();
	}

	public static void finishLoader() {
		if (loader == null) return;
		if (loader.activity.isFinishing()) return;

		loader.cancel();
		loader = null;
	}

	public static String fetchRedirectUrl(String urlStr) {
		try {
			URL url = new URL(urlStr);

			URLConnection conn = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) conn;
			//httpConn.setInstanceFollowRedirects(false);
			conn.connect();
			conn.getInputStream();

			String finishUrl = conn.getURL().toString();
			return finishUrl;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String fetchResponseUrl(String urlStr) {
		try {
			URL url = new URL(urlStr);

			BufferedReader in = new BufferedReader(
				new InputStreamReader(url.openStream()));

			StringBuilder sb = new StringBuilder();
			String inputLine;
			while ((inputLine = in.readLine()) != null)
				sb.append(inputLine);
			in.close();
			return sb.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	public static String getParam(String url, String param) {
		Uri uri = Uri.parse(url);
		return uri.getQueryParameter(param);
	}

	public static boolean isNot0(String str) {
		return !(str == null || str.isEmpty() || "0".equals(str));
	}


	public static WebView initWebView(Activity activity, WebView webView, OnDataCallback<String> tokenCallback) {
		webView.setWebChromeClient(new WebChromeClient());
		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);


		JavaScriptInterface myjs = new JavaScriptInterface(activity, tokenCallback);
		webView.addJavascriptInterface(myjs, "JSInterface");

		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				view.loadUrl("javascript:window.JSInterface.doSomething(window.location.href);");
			}
		});

		return webView;
	}


	public static WebView initWebView(Activity activity, OnDataCallback<String> tokenCallback) {
		WebView webView = activity.findViewById(R.id.webView);

		return initWebView(activity, webView, tokenCallback);
	}

	public static WebView initNewWebView(Activity activity, OnDataCallback<String> tokenCallback) {
		final WebView webView = new WebView(activity);

		webView.setWebChromeClient(new WebChromeClient());
		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);


		JavaScriptInterface myjs = new JavaScriptInterface(activity, tokenCallback);
		webView.addJavascriptInterface(myjs, "JSInterface");

		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				view.loadUrl("javascript:window.JSInterface.doSomething(window.location.href);");
			}
		});

		return webView;

	}

	public static boolean isPhoneValid(String phone) {
		return phone != null && phone.matches("^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$");
	}
}
