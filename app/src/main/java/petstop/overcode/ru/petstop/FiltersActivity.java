package petstop.overcode.ru.petstop;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Spinner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import petstop.overcode.ru.petstop.dto.AnimalType;
import petstop.overcode.ru.petstop.dto.FilterDto;
import petstop.overcode.ru.petstop.hook.AutocomleteUtil;
import petstop.overcode.ru.petstop.service.GetDataFunc;
import petstop.overcode.ru.petstop.service.LocationService;
import petstop.overcode.ru.petstop.service.MetroService;
import petstop.overcode.ru.petstop.util.AuthUtil;

public class FiltersActivity extends BaseActivity {
	public static FilterDto filter = new FilterDto();
	private AutoCompleteTextView city_autocomplete;
	private AutoCompleteTextView street_autocomplete;
	private AutoCompleteTextView metro_autocomplete;
	private AnimalType[] animalType;
	private Map<AnimalType, View> animalBtns;

	public static void addBehavior(final AnimalType[] animalType, final Map<AnimalType, View> animalBtns) {
		for (final AnimalType type : animalBtns.keySet()) {
			View btn = animalBtns.get(type);
			if (btn == null) return;
			btn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					animalType[0] = type;
					for (final View otherBtn : animalBtns.values()) {
						otherBtn.setBackgroundResource(R.drawable.disabled_button);
						((Button) otherBtn).setTextColor(Color.BLACK);
					}

					view.setBackgroundResource(R.drawable.enter_button);
					((Button) view).setTextColor(Color.WHITE);
				}
			});
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (!AuthUtil.CheckLogin(this)) return;

		setContentView(R.layout.activity_filters);

		city_autocomplete = (AutoCompleteTextView) findViewById(R.id.city_autocomplete);
		street_autocomplete = (AutoCompleteTextView) findViewById(R.id.street_autocomplete);
		metro_autocomplete = (AutoCompleteTextView) findViewById(R.id.metro_autocomplete);

		AutocomleteUtil.setAutocomplete(this, R.id.city_autocomplete, new GetDataFunc<HashSet<String>, String>() {
			@Override
			public HashSet<String> call(String txt) {
				return LocationService.getCityList(txt);
			}
		});
		AutocomleteUtil.setAutocomplete(this, R.id.street_autocomplete, new GetDataFunc<HashSet<String>, String>() {
			@Override
			public HashSet<String> call(String txt) {
				return LocationService.getStreetList(city_autocomplete.getText().toString(), txt);
			}
		});
		AutocomleteUtil.setAutocomplete(this, R.id.metro_autocomplete, new GetDataFunc<HashSet<String>, String>() {
			@Override
			public HashSet<String> call(String txt) {
				return new HashSet<>(MetroService.getMetro(city_autocomplete.getText().toString(), txt));
			}
		});

		findViewById(R.id.gotoBackBtn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		animalType = new AnimalType[1];
		animalBtns = new HashMap<AnimalType, View>() {{
			put(AnimalType.All, findViewById(R.id.all_btn));
			put(AnimalType.Dog, findViewById(R.id.dog_btn));
			put(AnimalType.Cat, findViewById(R.id.cat_btn));
		}};
		addBehavior(animalType, animalBtns);

		setFields();

		Button filtersBtn = (Button) findViewById(R.id.apply_btn);
		filtersBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				filter = new FilterDto() {{
					setCity(getAutoComplete(R.id.city_autocomplete));
					setStreet(getAutoComplete(R.id.street_autocomplete));
					setMetro(getAutoComplete(R.id.metro_autocomplete));
					setPriceMin(Integer.parseInt(getSpinnerItem(R.id.price_min_spinner)));
					setPriceMax(Integer.parseInt(getSpinnerItem(R.id.price_max_spinner)));
					setMinRating((int) ((RatingBar) findViewById(R.id.rating)).getRating());
					setAnimalType(animalType[0]);
					setMinDays(Integer.parseInt(getSpinnerItem(R.id.min_days_spinner)));
				}};

				finish();
			}
		});

		findViewById(R.id.reset_btn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				filter = new FilterDto() {
				};
				setFields();
				finish();
			}
		});
	}

	private void setFields() {
		city_autocomplete.setText(filter.getCity());
		street_autocomplete.setText(filter.getStreet());
		metro_autocomplete.setText(filter.getMetro());

		animalType[0] = filter.getAnimalType();

		setSpinnerItem(R.id.price_min_spinner, "" + filter.getPriceMin());
		setSpinnerItem(R.id.price_max_spinner, "" + filter.getPriceMax());
		setSpinnerItem(R.id.min_days_spinner, "" + filter.getMinDays());

		((RatingBar) findViewById(R.id.rating)).setRating((float) filter.getMinRating());

		animalBtns.get(filter.getAnimalType()).callOnClick();
	}

	private String getSpinnerItem(int spinnerId) {
		Spinner spinner = (Spinner) findViewById(spinnerId);
		return spinner == null ? "" :
			spinner.getSelectedItem() + "";
	}

	private void setSpinnerItem(int spinnerId, String val) {
		if (val == null || val.isEmpty()) return;

		Spinner spinner = (Spinner) findViewById(spinnerId);
		int pos = ((ArrayAdapter) spinner.getAdapter()).getPosition(val);
		if (pos < 0) return;

		spinner.setSelection(pos);
	}

	private String getAutoComplete(int autoCompleteId) {
		AutoCompleteTextView autoComplete = (AutoCompleteTextView) findViewById(autoCompleteId);
		return autoComplete.getText().toString();
	}

}
