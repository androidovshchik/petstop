package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.util.AuthUtil;
import petstop.overcode.ru.petstop.util.CustomHint;
import petstop.overcode.ru.petstop.util.U;

public class RecoveryPasswordActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recovery_password);

		findViewById(R.id.goToBackBtnRecovery).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		final EditText emailInput = (EditText) findViewById(R.id.emailInput);

		CustomHint customHint = new CustomHint("Введите адрес электронной почты", Typeface.ITALIC, 35f);
		emailInput.setHint(customHint);

		final Activity activity = this;
		Button recoveryPasswordButton = (Button) findViewById(R.id.recoveryPasswordButton);
		recoveryPasswordButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String email = emailInput.getText().toString().trim().toLowerCase();
				if (email.isEmpty()) return;

				U.startLoader(activity, "Письмо с восстановлением пароля отправлено на почту");
				AuthUtil.resetPassword(email, new OnDataCallback<Void>(activity) {
					@Override
					public void action(Void data) {
						U.finishLoader();
						finish();
					}
				});
			}
		});
	}
}
