package petstop.overcode.ru.petstop;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class RegisterAsCustomer extends RegisterAsZoo {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		((ViewGroup) metro_autocomplete.getParent().getParent()).removeView((View) metro_autocomplete.getParent());
		metro_autocomplete = null;

		((ViewGroup) street_autocomplete.getParent().getParent()).removeView((View) street_autocomplete.getParent());
		street_autocomplete = null;

		((ViewGroup) days_spinner.getParent().getParent()).removeView((View) days_spinner.getParent());
		days_spinner = null;
		price_spinner = null;

		findViewById(R.id.these_data_are_needed_to_communicate_with_the_zoo).setVisibility(View.VISIBLE);

		Button catBtn = (Button) findViewById(R.id.cat_btn);
		((ViewGroup) catBtn.getParent().getParent()).removeView((View) catBtn.getParent());
		findViewById(R.id.toolbarInProfile).setVisibility(View.INVISIBLE);
	}

	@Override
	public boolean isZoo() {
		return false;
	}

	@Override
	public boolean isEdit() {
		return false;
	}
}
