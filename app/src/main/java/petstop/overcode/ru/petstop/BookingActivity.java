package petstop.overcode.ru.petstop;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;

import petstop.overcode.ru.petstop.dto.MyUser;
import petstop.overcode.ru.petstop.dto.P2PDebitResult;
import petstop.overcode.ru.petstop.service.OnDataCallback;
import petstop.overcode.ru.petstop.service.PayService;
import petstop.overcode.ru.petstop.service.UserService;
import petstop.overcode.ru.petstop.util.U;

public class BookingActivity extends BaseActivity {
	private static String getDaysStr(int days) {
		days = days % 100;
		if (days >= 11 && days <= 19) {
			return "дней";
		} else {
			int i = days % 10;
			switch (i) {
				case (1):
					return "день";
				case (2):
				case (3):
				case (4):
					return "дня";
				default:
					return "дней";
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final MyUser user = UserService.currentUser;

		setContentView(R.layout.activity_booking);

		final Activity thisActivity = this;

		final int[] totalPrice = new int[1];
		final int[] daysArr = new int[1];


		// daysForBookingSpinner
		final Spinner daysForBookingSpinner = (Spinner) findViewById(R.id.daysForBookingSpinner);

		ArrayList<String> daysForNewDaySpinner = new ArrayList<>();
		int zooDays = ProfileActivity.zoo.getMinDays();
		SpinnerAdapter oldAdapter = daysForBookingSpinner.getAdapter();
		for (int i = 0; i < oldAdapter.getCount(); i++) {
			String s = oldAdapter.getItem(i).toString();
			if (Integer.parseInt(s) <= zooDays) {
				daysForNewDaySpinner.add(s);
			}
		}
		daysForBookingSpinner.setAdapter(
			new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
				daysForNewDaySpinner.toArray(new String[daysForNewDaySpinner.size()])));

		daysForBookingSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				String days = (String) daysForBookingSpinner.getAdapter().getItem(position);
				daysArr[0] = Integer.parseInt(days);
				totalPrice[0] = daysArr[0] * ProfileActivity.zoo.getPrice();
				((CustomTextView) findViewById(R.id.costBookingTextView)).setText(totalPrice[0] + "");

				U.startLoader(thisActivity, "Загрузка");
				AsyncTask.execute(new Runnable() {
					@Override
					public void run() {
						final String url = PayService.PayForBooking(user.getUuid(), totalPrice[0] * 100, user.getEmail(),
							user.getCardToken());
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								U.initWebView(thisActivity, GetOnTokenCallback(daysArr, user.getName())).loadUrl(url);
							}
						});
					}
				});
			}

			public void onNothingSelected(AdapterView<?> parentView) {
			}
		});
		// end of daysForBookingSpinner


		findViewById(R.id.gotoBackBtnBooking).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
	}

	private OnDataCallback<String> GetOnTokenCallback(final int[] daysArr, final String userName) {
		final Activity thisActivity = this;
		return new OnDataCallback<String>(this) {
			@Override
			public void action(final String url) {

				if (url.contains("my.php")) {
					if (U.isNot0(U.getParam(url, "operation"))) {
						AsyncTask.execute(new Runnable() {
							@Override
							public void run() {
								final String status = PayService.GetStatus(U.getParam(url, "id"), U.getParam(url, "operation"));
								if (!"APPROVED".equals(status)) {
									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											U.longToast(thisActivity, "Ошибка оплаты. " + status);
										}
									});

									try {
										Thread.sleep(3500);
									} catch (InterruptedException e) {
									}
									thisActivity.finish();
								} else {
									PayService.sendFromAdmin(ProfileActivity.zoo.getEmail(), "PetStop",
										"Ваши услуги на " + daysArr[0] + " " + getDaysStr(daysArr[0]) + " забронировал " + userName);

									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											P2PDebitResult result = new P2PDebitResult();
											result.setOrderId(U.getParam(url, "id"));
											result.setOperationId(U.getParam(url, "operation"));
											saveInDb(result, thisActivity);

											U.longToast(thisActivity, "Зооняня забронирована");
										}
									});
								}
							}
						});
					} else {
						U.longToast(thisActivity, "Ошибка сервиса оплаты");
					}
				} else {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							U.finishLoader();
						}
					});
				}
			}
		};
	}

	private void saveInDb(P2PDebitResult result, final Activity thisActivity) {
		UserService.setBooked(ProfileActivity.zoo, result, new OnDataCallback<Void>(thisActivity) {
			@Override
			public void action(Void data) {
				U.finishLoader();
				final Dialog dialog = new Dialog(thisActivity);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setCancelable(true);
				dialog.setContentView(R.layout.activity_reminder_dialog);
				dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialogInterface) {
						thisActivity.finish();
					}
				});
				dialog.show();
			}
		});
	}
}
